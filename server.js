var express = require('express');
var index = require('hours-api');

module.exports = function (mongodb, config, port, cb) {

  var app = express();

  app.use(express.static(__dirname + '/www'));
  app.use(index(mongodb, config));

  var server = app.listen(port, function (err) {
    if (err) return cb(err);
    console.log("Hours api is running at localhost:" + port);
    cb(null, server);
  });

};
