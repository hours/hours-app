describe('Approve-all', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('Approve_All_Multiple_Pending_Shifts', function (err) {
      done(err)
    });
  });

  it('should login with owner', function (done) {
     helper.token("100000000000000000000000", function (err) {
       done(err)
     });
  });

describe('owner approve -all', function () {
  it('owner should be able to approve-all', function () {

    browser.get('/index.html#/index');

    //element(by.css('body > ion-nav-view > ion-tabs > ion-nav-view > ion-view > ion-content > div.scroll > ion-list > div > ion-item:nth-child(2) > div.item-content')).click();
    var owner = element(by.css('ion-content ion-list > div > ion-item > div'));

    // open owner sheet
    owner.click();

    var pending = element.all(by.cssContainingText('p.status-pending', 'pending'));
    expect(pending.count()).toBe(3);


    var appr_all=element(by.css('[ng-click="approveAll()"]'));
    appr_all.click();


    var el = element.all(by.cssContainingText('p.status-approved', 'approved'));
    expect(el.count()).toBe(3);
    expect(pending.count()).toBe(0);


  });
});




});
