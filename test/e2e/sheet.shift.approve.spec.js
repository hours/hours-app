describe('Shifts', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('OneOrganisation_TwoUsers', function (err) {
      done(err)
    });
  });

  describe('Shifts', function () {

    it('should login with worker', function (done) {
      helper.token("100000000000000000000001", function (err) {
        if (err) return done(err);
          done()
      });
    });

    it('should create new shift', function () {

      browser.get('/index.html#/index');

      var firstRow = element(by.css('#sheet ion-content ion-list:nth-child(2)'));

      // open model
      firstRow.element(by.css('ion-item button')).click();

      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      // set time
      element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
      element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
      element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
      element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

      // save
      element(by.css('ion-modal-view #sheetShiftSave')).click();

      // expect
      expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('10:00 - 14:00')


    });

    it('should login with owner', function (done) {
      helper.token("100000000000000000000000", function (err) {
        if (err) return done(err);

        browser.setLocation('index').then(function () {
          done()
        });
      });
    });

    it('should approve shift', function () {

      browser.get('/index.html#/index');

      element(by.css('body > ion-nav-view > ion-tabs > ion-nav-view > ion-view > ion-content > div.scroll > ion-list > div > ion-item:nth-child(2) > div.item-content')).click();

      var firstRow = element(by.css('#sheet ion-content ion-list:nth-child(2)'));

      // approve shift
      firstRow.element(by.css('ion-item button')).click();

      // expect
      expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('10:00 - 14:00')

    });
  });

});

