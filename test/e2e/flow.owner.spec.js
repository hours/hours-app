describe('Flow owner', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err)
    })
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  // load data
  beforeEach(function (done) {
    helper.data('OneOrganisation_TwoUsers', function (err) {
      done(err)
    });
  });

  it('should login with owner', function (done) {
    helper.token("100000000000000000000000", function (err) {
      done(err)
    });
  });


  it('should have right flow and display employees ', function () {
    browser.get('/index.html#/index');
    var header = element(by.css("ion-header-bar > div > span"));
    expect(header.getText()).toBe("Venster 33");
  });
/*@todo
  it('owner should be able to edit employees ', function () {

  });

  it('owner should be able to delete employees ', function () {

  });
*/

  it('owner should be able to ADD hours and get status approved', function () {

    browser.get('/index.html#/index');

    //element(by.css('body > ion-nav-view > ion-tabs > ion-nav-view > ion-view > ion-content > div.scroll > ion-list > div > ion-item:nth-child(2) > div.item-content')).click();
    var owner = element(by.css('ion-content ion-list > div > ion-item > div'));

    // open owner sheet
    owner.click();

    // open model
    var firstRow = element(by.css('#sheet ion-content ion-list:nth-child(2)'));
    firstRow.element(by.css('ion-item button')).click();

    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    // set time
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

    // save
    element(by.css('ion-modal-view #sheetShiftSave')).click();

    // expect
    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('10:00 - 14:00')
    expect(firstRow.element(by.css('p.status-approved')).getText()).toBe('approved');


    //re-open to EDIT
    element(by.css('div.buttons > button.button-approved')).click();
    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    // set time
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

    // save
    element(by.css('ion-modal-view #sheetShiftSave')).click();

    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('08:00 - 16:00')

    //re-open to DELETE
    element(by.css('div.buttons > button.button-approved')).click();
    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    //delete
    element(by.css('ion-modal-view #sheetShiftDelete')).click();
    element(by.css('div.popup-container button.button-positive')).click();

    // expect
    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('29 December');
  });




});
