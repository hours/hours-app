//share report via cvs
//share report via excel
//refresh report does not change hours


//login as owner - go to reports - create report - report should be there
describe('functionalities tab reports', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('OneOrganisation_MultipleUsers_OneApprover', function (err) {
      done(err)
    });
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  describe('reports as owner', function () {

      it('should login with owner', function (done) {
        helper.token("100000000000000000000000", function (err) {
          done(err)
        });
      });


      it('should have right flow and display employees ', function () {
        browser.get('/index.html#/index');
        var header = element(by.css("ion-header-bar > div > span"));
        expect(header.getText()).toBe("Venster 33");
      });

      it('owner should be able to add reports', function () {
         //tab reports is on
         var rep=element(by.css('i.icon.ion-ios-list-outline'));
         expect(rep.isDisplayed()).toBe(true);
         //go to reports
         rep.click();

         //add report
         var add=element(by.css('[ng-click="generate()"]'));
         add.click();

         //select month
         element(by.cssContainingText('option', 'July')).click();
         element(by.cssContainingText('option', '2016')).click();
         element(by.css('button.ng-binding.button-positive')).click();

      });

      it('expect report to be there and click for details', function () {
        //report is there and can be clicked
        var report=element(by.css('[ng-click="goDetail(report)"]'));
        report.click();


        var uur=element(by.css('div.title.title-center.header-item'));
        expect(uur.getText()).toBe('Report');

        var h2=element.all(by.repeater('item in list')).get(0).getText();
        expect(h2).toContain('2.00 h');

      });

  });


  describe('reports as approver', function () {

      it('should login with owner', function (done) {
        helper.token("100000000000000000000002", function (err) {
          done(err)
        });
      });


      it('should not be able to see tab reports', function () {
         //tab reports is on
         browser.get('/index.html');
         var rep=element(by.css('i.icon.ion-ios-list-outline'));
         expect(rep.isDisplayed()).toBe(false);
      });
  });

  describe('reports as employee', function () {

        it('should login with owner', function (done) {
          helper.token("100000000000000000000003", function (err) {
            done(err)
          });
        });


        it('should not be able to see tab reports', function () {
           //tab reports is on
           browser.get('/index.html');
           var rep=element(by.css('i.icon.ion-ios-list-outline'));
           expect(rep.isDisplayed()).toBe(false);
        });
    });

});
