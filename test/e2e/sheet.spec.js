describe('Shifts', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    });
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  // data
  beforeEach(function (done) {
    helper.data('OneOrganisation_OneUser', function (err) {
      if (err) done(err);
      done(err);
    });
  });

  // token
  beforeEach(function (done) {
    helper.token("100000000000000000000000", function (err) {
      if (err) done(err);
      browser.refresh().then(function (err) {
        done(err);
      });
    });
  });


  it('should display header name in ', function () {

    browser.get('/index.html#/index');

    var header = element(by.css('body > ion-nav-bar > div:nth-child(2) > ion-header-bar'));
    expect(header.getText()).toBe("Venster 33");
  });
});
