var moment = require('moment');

describe('flow approver', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('OneOrganisation_MultipleUsers_OneApprover', function (err) {
      done(err)
    });
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  describe('Flow approver - login as approver', function () {


    it('should login with approver', function (done) {
      helper.token("100000000000000000000002", function (err) {
        if (err) return done(err);
        browser.setLocation('/tab/settings').then(function () {
          done();
        });
      });
    });

    it('should be approver in settings', function () {
      //browser.pause()
      var name = element(by.css('ion-content > div > ion-list > div > div > h2'));
      expect(name.getText()).toBe('Jasmin Jane');
    });

    it('should login with approver', function (done) {
      helper.token("100000000000000000000002", function (err) {
        if (err) return done(err);
        browser.setLocation('index').then(function () {
          done();
        });
      });
    });


    it('should be in approver flow', function () {
      var header = element(by.css("ion-header-bar > div > span"));
      expect(header.getText()).toBe("Venster 33");
    });

    it('should see two employees plus me', function () {
      var employees = element.all(by.repeater('employee in employees'));
      expect(employees.count()).toEqual(3);

    });


    it('should be able to add hours', function () {
      var me = element(by.cssContainingText('.item-content', 'Jasmin Jane'));
      me.click();

      var firstRow = element(by.css('#sheet ion-content ion-list:nth-child(2)'));
      firstRow.element(by.css('ion-item button')).click();

      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      // set time
      element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
      element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
      element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
      element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

      // save
      element(by.css('ion-modal-view #sheetShiftSave')).click();

      // expect hours to be pending
      expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('10:00 - 14:00')
      expect(firstRow.element(by.css('p.status-pending')).getText()).toBe('pending');

    });



    it('should login with approver', function (done) {
          helper.token("100000000000000000000002", function (err) {
            if (err) return done(err);
            browser.setLocation('/tab/settings').then(function () {
              done();
            });
          });
    });

    it('should be able to reject hours as approver', function () {
      browser.setLocation('index');
      var employee = element(by.cssContainingText('.ng-binding', 'Test'));
      employee.click();

      //reject shift as approver
      var reject = element(by.css('button.button-rejected'));
      reject.click();
      expect(element(by.css('p.status-rejected')).getText()).toBe('rejected');
    });


});

 ///test if reject worked:

 describe('Flow approver-login as worker', function () {
   it('should login with worker', function (done) {
          helper.token("100000000000000000000003", function (err) {
            if (err) return done(err);
            browser.setLocation('index').then(function () {
              done()
            });
          });
    });


     it('should have status rejected', function () {
       //browser.setLocation('/tab/settings');
       //browser.pause();
       browser.setLocation('index')
       //go straight to sheets
       expect(element(by.css('p.status-rejected')).getText()).toBe('rejected');

     });
 })

});



