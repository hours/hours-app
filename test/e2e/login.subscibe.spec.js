describe('Shifts', function () {

  var helper;

  // setup
  beforeAll(function (done) {
    helper = new require('../helper')(browser);
    helper.database(function (err) {
      if (err) return done(err);
      helper.server(function (err) {
        if (err) return done(err);
        browser.get('/#/login').then(function () {
          done(err);
        });
      });
    })
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  beforeEach(function (done) {
    helper.data('OneOrganisation_OneUser', function (err) {
      done(err)
    });
  });


  it('should signup for a new account', function () {
    element(by.css('#loginView .button-signin')).click();

    var form = element(by.css('#loginSubscribeView > ion-content > div.scroll > form'));

    form.element(by.model('item.firstname')).sendKeys('Willem');
    form.element(by.model('item.lastname')).sendKeys('Veelenturf');
    form.element(by.model('item.email')).sendKeys('willem.veelenturf@gmail.com');
    form.element(by.model('item.password')).sendKeys('willem');
    form.element(by.model('item.repassword')).sendKeys('willem');

    form.element(by.css('.button')).click();
  });

});
