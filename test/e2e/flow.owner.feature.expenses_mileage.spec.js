describe('Flow owner Feature expenses mileage', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err)
    })
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  // load data
  beforeEach(function (done) {
    helper.data('OneOrganisation_Feature_Expenses_Mileage', function (err) {
      done(err)
    });
  });

  it('should login with owner', function (done) {
    helper.token("100000000000000000000000", function (err) {
      done(err)
    });
  });


  it('owner should be able to ADD EXPENSES and get status approved', function () {

      browser.get('/index.html#/index');
      var owner = element(by.css('ion-content ion-list > div > ion-item > div'));

      // open owner sheet
      owner.click();

      // open model
      var firstRow = element.all(by.repeater('(date, timesheet) in timesheets')).get(0)
      firstRow.element(by.css('ion-item button')).click();

      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      // click on expenses
      element(by.css('.icon.ion-ios-copy-outline')).click();


      //add expenses
      var form = element(by.name('expensesForm'))
      form.element(by.model('data.amount')).sendKeys('1200');
      form.element(by.model('data.description')).click().sendKeys('Test add expenses');
      //todo: send project
      form.element(by.id('sheetExpensesSave')).click();


      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(1)')).getText()).toBe('$1,200.00');
      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(2)')).getText()).toBe('Test add expenses');

      //todo: expect project
      expect(firstRow.element(by.css('p.status-approved')).getText()).toBe('approved');

      //edit
      element(by.css('div.buttons > button.button-approved')).click();
      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      form.element(by.model('data.amount')).click().sendKeys('5');
      form.element(by.model('data.description')).click().sendKeys(' edit');

      //enlarge window, otherwise element not found;

      var s = element(by.css('ion-modal-view #sheetExpensesSave'));
      s.click();

      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(1)')).getText()).toBe('$51,200.00');
      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(2)')).getText()).toBe('Test add expenses edit');

      //delete
      element(by.css('div.buttons > button.button-approved')).click();
      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      element(by.id('sheetExpensesDelete')).click();
      element(by.css('div.popup-container button.button-positive')).click();

      expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('29 December');

    });


it('owner should be able to ADD MILEAGE and get status approved', function () {

      browser.get('/index.html#/index');

      //element(by.css('body > ion-nav-view > ion-tabs > ion-nav-view > ion-view > ion-content > div.scroll > ion-list > div > ion-item:nth-child(2) > div.item-content')).click();
      var owner = element(by.css('ion-content ion-list > div > ion-item > div'));

      // open owner sheet
      owner.click();

      // open model
      var firstRow = element.all(by.repeater('(date, timesheet) in timesheets')).get(0)
      firstRow.element(by.css('ion-item button')).click();

      // check model open
      expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

      // click on expenses
      element(by.css('.icon.ion-ios-speedometer-outline')).click();


      //add expenses
      var form = element(by.name('mileageForm'))
      form.element(by.model('data.distance')).sendKeys('150');
      form.element(by.model('data.origin')).click().sendKeys('Amsterdam');
      form.element(by.model('data.destination')).click().sendKeys('Utrecht');

      //todo: send project
      form.element(by.id('sheetMileageSave')).click();


      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(1)')).getText()).toBe('150 Km');
      expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(2)')).getText()).toBe('Amsterdam-Utrecht');
      //todo: expect project
      expect(firstRow.element(by.css('p.status-approved')).getText()).toBe('approved');


     //edit
     element(by.css('div.buttons > button.button-approved')).click();
     // check model open
     expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

     form.element(by.model('data.distance')).click().sendKeys('5');
     form.element(by.model('data.origin')).click().sendKeys(' edit');

     //enlarge window, otherwise element not found;

     var m = element(by.css('ion-modal-view #sheetMileageSave'));
     m.click();

     expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(1)')).getText()).toBe('5150 Km');
     expect(firstRow.element(by.css('div.col.col-time.small p:nth-child(2)')).getText()).toBe('Amsterdam edit-Utrecht');

     //delete
     element(by.css('div.buttons > button.button-approved')).click();
     // check model open
     expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

     element(by.id('sheetMileageDelete')).click();
     element(by.css('div.popup-container button.button-positive')).click();

     expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('29 December');


    });


});
