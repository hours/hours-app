//login as owner - go to reports - create report - report should be there
describe('functionalities tab settings', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('OneOrganisation_MultipleUsers_OneApprover', function (err) {
      done(err)
    });
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  var swipe = function(el, distance) {
    browser.driver
        .actions()
        .mouseDown(el)
        .mouseMove({
            x: distance,
            y: 0
        })
    .mouseUp().perform();
  };


  browser.swipeLeft = function(el, distance) {
      swipe(el, distance);
  };

  browser.swipeRight = function(el, distance) {
      swipe(el, distance);
  };


  describe('settings as owner', function () {

      it('should login with owner', function (done) {
        helper.token("100000000000000000000000", function (err) {
          done(err)
        });
      });


      it('owner should see and edit user profile', function () {

         browser.setLocation('/tab/settings');

         //profile
         var usr=element(by.css('[ng-click="goUser()"]'));
         expect(usr.element(by.css('h2')).getText()).toBe('Willem Veelenturf 1');

         //user settings
         usr.click();


         //edit user settings
         element(by.model('user.name')).click().sendKeys('2');
         element(by.model('user.email')).click().sendKeys('2');
         //save
         element(by.id('saveUser')).click();

         //check
         expect(usr.element(by.css('h2')).getText()).toBe('Willem Veelenturf 12');

         //to user settings
         usr.click();



         expect(element(by.model('user.name')).getAttribute('value')).toBe('Willem Veelenturf 12');
         expect(element(by.model('user.email')).getAttribute('value')).toBe('willem.veelenturf1@gmail.com2');

         //var back=element(by.css('button.back-button.buttons.button-clear.header-item'));
         //back.click();


      });

      it('owner should see org, be able to create org and edit and delete org', function () {
         //linked project
         browser.setLocation('/tab/settings');
         var org=element(by.css('[ng-click="goOrganisations()"]'));
         org.click();

         //add organisation

         var add=element(by.css('[ng-click="add()"]'));
         add.click();
         var frm = element(by.name('orgForm'));
         frm.element(by.model('organisation.name')).click().sendKeys('ProtractorOrg');
         frm.element(by.model('organisation.description')).click().sendKeys('ProtractorOrg');
         frm.element(by.model('organisation.settings.multipleshifts')).element(by.xpath("..")).click();
         frm.element(by.model('organisation.settings.pause')).element(by.xpath("..")).click();
         frm.element(by.model('organisation.settings.availability')).element(by.xpath("..")).click();

         //save new organisation
         var save=element(by.css('button.button.ng-binding.button-positive'));
         save.click();

         //can find organisation
         element.all(by.repeater('organisation in organisations')).then(function(organisations) {
           var titleElement = organisations[0].element(by.css('h2'));
           expect(titleElement.getText()).toEqual('ProtractorOrg');


           //@todo, find out how to swipe. swipe(titleElement, -1000);

           //edit, need swipe
           //delete, need swipe

           //find in settings

           titleElement.click();
           var org=element(by.css('[ng-click="goOrganisations()"] > h2'));
           expect(org.getText()).toBe('ProtractorOrg');

         });


      });

      it('owner should see approvers and be able to assign approvers', function () {
      browser.setLocation('/tab/settings');
        var approver=element(by.css('[ng-click="goApprover()"]'));
        approver.click();



        //see 1 employee
        var cnt = element.all(by.repeater('employee in employees'));
        expect(cnt.count()).toEqual(5);

        //change organisation in scope
        browser.setLocation('/tab/settings');
        var org=element(by.css('[ng-click="goOrganisations()"]'));
        org.click();
        element.all(by.repeater('organisation in organisations')).then(function(organisations) {
          organisations[1].click();
        });

        //see 4 employees

        approver.click();
        var cnt2 = element.all(by.repeater('employee in employees'));
        expect(cnt2.count()).toEqual(8);

        //@todo: assign approver


        //@todo: willem 2 is now approver on jasmin jane, check
      });

      it('owner should see projects and be able to create edit and delete projects and assign employees', function () {
      browser.setLocation('/tab/settings');

        var projects=element(by.css('[ng-click="goProjects()"]'));
        projects.click();
        var add=element(by.css('[ng-click="add()"]'));
        add.click();
        element(by.model('project.name')).click().sendKeys('TestProject');
        element(by.model('project.code')).click().sendKeys('123');
        //save
        element.all(by.repeater('button in buttons')).then(function(buttons) {
           buttons[1].click();
        });
        //find project
        element.all(by.repeater('project in projects')).then(function(projects) {
           expect(projects[0].getText()).toBe('TestProject - 123');
           //assign employees
           projects[0].click();

           //@todo finish
           //browser.pause();

        });



        //@todo: swipe - edit and delete
      });



      it('owner should see credits and be able to see transactions', function () {


      });

      //@todo total settings for owner:
      //it('owner should 8 items in settings list', function () {
      //  browser.get('/#/tab/settings');
      //  var aantal = element.all(by.css('ion-list > div > div'));
      //  expect(aantal.count()).toEqual(8);
      //});

  });


  describe('settings as approver', function () {

      it('should login with owner', function (done) {
        helper.token("100000000000000000000002", function (err) {
          done(err)
        });
      });

  });

  describe('settings as employee', function () {

        it('should login with owner', function (done) {
          helper.token("100000000000000000000003", function (err) {
            done(err)
          });
        });
   });


});
