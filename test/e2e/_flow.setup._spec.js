//var moment = require('moment');

describe('flow setup', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err);
    })
  });

  // load data
  beforeAll(function (done) {
    helper.data('No_Organisation', function (err) {
      done(err)
    });
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  describe('Welcome page', function () {

    it('should login with user no orgs', function (done) {
      helper.token("100000000000000000000000", function (err) {
        if (err) return done(err);
        browser.setLocation('/tab/settings').then(function () {
          done();
        });
      });
    });

     it('should be approver in settings', function () {
          //browser.pause()
          var name = element(by.css('ion-content > div > ion-list > div > div > h2'));
          expect(name.getText()).toBe('Jasmin Jane');
        });

    it('should have no organisations', function () {
      browser.pause();
      var title = element(by.css('ion-content h1'));
      expect(title.getText()).toBe('Welcome');

      var title = element(by.css('ion-content h1'));
      expect(title.getText()).toBe('Welcome');

      var title = element(by.css('ion-content h1'));
      expect(title.getText()).toBe('Welcome');
    });

  });

  describe('Join organisation', function () {

    it('should login with approver', function (done) {
      helper.token("100000000000000000000000", function (err) {
        if (err) return done(err);
        browser.refresh().then(function () {
          done();
        });
      });
    });

    it('should be approver in settings', function () {
      element(by.css('#setup-join')).click();
    });

  });


});



