describe('Flow Employee', function () {

  var helper = new require('../helper')(browser);

  // setup
  beforeAll(function (done) {
    browser.get('/index.html');
    helper.setup(function (err) {
      done(err)
    })
  });

  afterAll(function (done) {
    helper.clear(function (err) {
      done(err);
    });
  });

  // load data
  beforeEach(function (done) {
    helper.data('OneOrganisation_TwoUsers', function (err) {
      done(err)
    });
  });

  it('should login with owner', function (done) {
    helper.token("100000000000000000000001", function (err) {
      done(err)
    });
  });


// --- SHEETS -- //

  it('should have right flow and display sheet ', function () {
    browser.get('/index.html#/index');
    var header = element(by.css("div.date > p:nth-child(2)"));
    expect(header.getText()).toBe("Week 01");
  });

  it('employee should be able to ADD hours and get status pending', function () {

    browser.get('/index.html#/index');

    // open model
    var firstRow = element(by.css('#sheet ion-content ion-list:nth-child(2)'));
    firstRow.element(by.css('ion-item button')).click();

    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    // set time
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

    // save
    var shiftsave = element(by.css('ion-modal-view #sheetShiftSave'));
    shiftsave.click();

    // expect
    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('10:00 - 14:00')
    expect(firstRow.element(by.css('p.status-pending')).getText()).toBe('pending');


    //re-open to EDIT
    element(by.css('div.buttons > button.button-edit')).click();
    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    // set time
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #fromDatePicker .decreaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();
    element(by.css('ion-modal-view #toDatePicker .increaseHours')).click();

    // save
    element(by.css('ion-modal-view #sheetShiftSave')).click();

    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('08:00 - 16:00')

    //re-open to DELETE
    element(by.css('div.buttons > button.button-edit')).click();
    // check model open
    expect(element(by.css('ion-modal-view > ion-header-bar h1')).getText()).toBe('29-12-1969');

    //delete
    element(by.css('ion-modal-view #sheetShiftDelete')).click();
    element(by.css('div.popup-container button.button-positive')).click();

    // expect
    expect(firstRow.element(by.css('div.col-time p:nth-child(1)')).getText()).toBe('29 December');
  });



// ----   SETTINGS    --- //


it('should display right settings for employee', function () {

   browser.get('/index.html#/index');
   //var sett = element(by.css('icon.ion-ios-gear-outline'));
   var sett=element(by.css('i.icon.ion-ios-gear-outline'));
   sett.click();

   //profile
   var usr=element(by.css('[ng-click="goUser()"]'));
   expect(usr.element(by.css('h2')).getText()).toBe('Willem Veelenturf 2');

   //user settings
   usr.click();
   expect(element(by.css('div.title.title-center.header-item')).getText()).toBe('User Settings');

   //edit user settings
   element(by.model('user.name')).click().sendKeys('2');
   element(by.model('user.email')).click().sendKeys('2');
   //save
   element(by.id('saveUser')).click();

   //check

   expect(usr.element(by.css('h2')).getText()).toBe('Willem Veelenturf 22');

   //to user settings
   usr.click();
   //browser.pause();
   expect(element(by.model('user.name')).getAttribute('value')).toBe('Willem Veelenturf 22');
   expect(element(by.model('user.email')).getAttribute('value')).toBe('willem.veelenturf2@gmail.com2');

   var back=element(by.css('button.back-button.buttons.button-clear.header-item'));
   back.click();

   //linked project
   var org=element(by.css('[ng-click="goOrganisations()"]'));
   expect(org.element(by.css('h2')).getText()).toBe('Venster 33');

   // count is 2 items, user settings and organisation
   var aantal = element.all(by.css('ion-list > div'));
   expect(aantal.count()).toEqual(8);


});

it('employee can add organisation and see the right settings', function () {

   browser.get('/index.html#/index');

   var sett=element(by.css('i.icon.ion-ios-gear-outline'));
   sett.click();

   //add organisation
   var org=element(by.css('[ng-click="goOrganisations()"]'));
   org.click();
   var add=element(by.css('[ng-click="add()"]'));
   add.click();
   var frm = element(by.name('orgForm'));
   frm.element(by.model('organisation.name')).click().sendKeys('ProtractorOrg');
   frm.element(by.model('organisation.description')).click().sendKeys('ProtractorOrg');
   frm.element(by.model('organisation.settings.multipleshifts')).element(by.xpath("..")).click();
   frm.element(by.model('organisation.settings.pause')).element(by.xpath("..")).click();
   frm.element(by.model('organisation.settings.availability')).element(by.xpath("..")).click();

   //save new organisation
   var save=element(by.css('button.button.ng-binding.button-positive'));
   save.click();

   //can find and edit organisation
   var cnt = element.all(by.repeater('organisation in organisations'));
   expect(cnt.count()).toEqual(2);

   //find and check scope
   element.all(by.repeater('organisation in organisations')).then(function(organisations) {
      var titleElement = organisations[0].element(by.css('h2'));
      expect(titleElement.getText()).toEqual('ProtractorOrg');

      titleElement.click();
      var org=element(by.css('[ng-click="goOrganisations()"] > h2'));
      expect(org.getText()).toBe('ProtractorOrg')

   });

   //changed organisation scope and is owner, so see more settings

   //find settings approver
   var appr=element(by.css('[ng-click="goApprover()"] > h2'));
   expect(appr.getText()).toBe('Approvers')
   //find settings projects
   var proj=element(by.css('[ng-click="goProjects()"] > h2'));
   expect(proj.getText()).toBe('Projects')
   //find settings transactions
   var trans=element(by.css('[ng-click="goTransactions()"] > h2'));
   expect(trans.getText()).toBe('Credits');



   //todo: should be able to add hours with pause and availability
   //browser.get('/index.html#/index');


});



});
