exports.config = {
  capabilities: {
    'browserName': 'chrome',
    'chromeOptions': {
      args: [
         '--disable-web-security',
         '--start-maximized'
      ],
    }
  },
  baseUrl: 'http://localhost:8888',
  specs: [
    'e2e/*.spec.js'
  ],
  onPrepare: function() {
        browser.driver.manage().window().maximize();
  },
  jasmineNodeOpts: {
    isVerbose: true
  }
};
