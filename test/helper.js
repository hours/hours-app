var fs = require("fs");
var async = require("async");
var moment = require("moment");

var jwt = require('jwt-simple');

var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

var config = require('../config.json');

var url;
var mongodb;
var server;

module.exports = function (browser) {


  if (!process.env.MONGO_URL)
    url = 'mongodb://localhost:27017/hours-regression';
  else
    url = process.env.MONGO_URL;

  // Get browser log
  //browser.manage().logs().get('browser').then(function (browserLog) {
  //  console.log(browserLog);
  //});

  browser.addMockModule('hours.mock', function () {
    angular.module('hours.mock', []).run(function () {
      (function (D) {
        Date = function () {
          if (arguments.length === 0) {
            return new D(0);
          }
          if (arguments.length === 1) {
            return new D(arguments[0]);
          }
          if (arguments.length === 7) {
            return new D(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], arguments[5], arguments[6]);
          }
        };
        Date.now = D.now;
        Date.UTC = D.UTC;
      })(Date);
    });
  });

  var helper = {

    setup: function (callback) {

      helper.database(function (err, mongodb) {
        if (err) return callback(err);
        helper.server(function (err, server) {
          if (err) return callback(err);
          callback(null, mongodb, server);
        });
      });
    },

    database: function (callback) {
      if (mongodb) return callback(null, mongodb);

      MongoClient.connect(url, function (err, db) {
        if (err) return callback(err);
        mongodb = db;
        callback(null, db);
      });
    },

    server: function (callback) {
      if (server) return callback(null, server);

      var app = require('../server');
      var port = 8888;
      app(mongodb, config, port, function (err, s) {
        if (err) return callback(err);
        server = s;
        callback(null, s)
      });
    },


    data: function (file, callback) {
      mongodb.dropDatabase();

      file = __dirname + '/../data/' + file + '.json';
      fs.readFile(file, function (err, data) {
        if (err) return callback(err);

        var obj = JSON.parse(data);

        var exec = [];
        Object.keys(obj).forEach(function (key) {
          exec.push(function (done) {
            var collection = mongodb.collection(key);

            var result = obj[key].map(function (val) {
              if (val._id) val._id = ObjectID(val._id);
              if (val.from) val.from = new Date(val.from);
              if (val.to) val.to = new Date(val.to);
              if (val.date) val.tate = new Date(val.date);

              return val;
            });

            collection.insert(result, function (err) {
              done();
            });
          })
        });

        async.parallel(exec, function () {
          callback(err);
        });
      });
    },

    token: function (user, callback) {

      var collection = mongodb.collection('users');

      collection.findOne(ObjectID(user), function (err, user) {
        if (err) callback(err);

        // create token
        var expires = moment().add(7, 'days').valueOf();
        var payload = {
          iss: user,
          exp: expires
        };

        var token = jwt.encode(payload, config.jwtTokenSecret);
        browser.executeScript('window.localStorage.setItem("access_token", "' + token + '");').then(function (err) {
          browser.refresh().then(function(){
            callback(null, token);
          })
        });

      });
    },

    clear: function (callback) {
      browser.executeScript('window.localStorage.clear();').then(function () {
        callback(null);
      });
    }

  };
  return helper;
};
