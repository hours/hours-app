var server = require('./server');
var config = require('./config.json');
var port = 8888;

var MongoClient = require('mongodb').MongoClient;

//process.env.MONGO_URL = 'mongodb://localhost:27017/hours-local';
process.env.MONGO_URL = 'mongodb://localhost:27017/hours-local';

MongoClient.connect(process.env.MONGO_URL, function (err, mongodb) {
  if(err) return console.error(err);
  server(mongodb, config, port, function (){});
});

