angular.module('hours.service.barcode', [])

  .factory("barcodeService", function ($q, tokenService) {

    return {
      scanQrcode: function () {
        return $q(function (resolve, reject) {
          if (window.cordova && window.cordova.plugins && window.cordova.plugins.barcodeScanner) {
            var options = {
              "prompt": "Place a barcode inside the scan area",
              "formats": "QR_CODE",
              "orientation": "portrait"
            };

            window.cordova.plugins.barcodeScanner.scan(
              function (result) {
                if (result.format === 'QR_CODE' && !result.cancelled) {
                  var match = result.text.match(/http:\/\/hours-app.com\/token.html\#\/(.*)\/(.*)/);
                  resolve({
                    action:match[1],
                    token:match[2]
                  });
                } else {
                  reject('Scanner: wrong format or cancelled: ' + result.format + ' - ' + !result.cancelled)
                }
              },
              function (error) {
                // TODO: plugin does send wrong errors
                //reject("Scanner: " + error)
              }, options
            );
          } else {
            var obj = {
              _id: '111111111111111111111111',
              name: 'Org name'
            };
            var token = tokenService.encode(obj)
            resolve(token)
          }
        });
      },

      createQrcode: function () {

        return $q(function (resolve, reject) {
          if (window.cordova && window.cordova.plugins.barcodeScanner) {
            cordova.plugins.barcodeScanner.encode(cordova.plugins.barcodeScanner.Encode.TEXT_TYPE, "http://www.nytimes.com", function (success) {
                alert("encode success: " + success);
              }, function (fail) {
                alert("encoding failed: " + fail);
              }
            );
          }
        })
      }
    }

  });
