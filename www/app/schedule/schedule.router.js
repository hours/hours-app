angular.module('hours.schedule.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('tab.schedule', {
        url: '/schedule',
        views: {
          'tab-schedule': {
            templateUrl: 'app/schedule/schedule.html',
            controller: 'ScheduleController'
          }
        }
      })

  });
