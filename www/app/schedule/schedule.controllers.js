angular.module('hours.schedule.controllers', [])

  .controller('ScheduleController', function ($q, $scope, $log, $http, $stateParams, $state, $ionicLoading, $ionicPopup, $timeout, cacheService, scheduleService, employeesService) {

    var date = moment.utc();

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init()
    });

    $scope.init = function () {

      $scope.date = date;

      $ionicLoading.show({
        template: 'Loading...'
      });

      cacheService.organisation().then(function (organisation) {

        $q.all({
          users: cacheService.employees(),
          projects: cacheService.projects(),
          categories: cacheService.categories(),
          shifts: scheduleService.findByDate(organisation._id, date.format('YYYY-MM-DD'))
        }).then(function (res) {

          var usersArr = {};
          res.users.forEach(function (user) {
            usersArr[user._id] = user.name
          });

          var projectsArr = {};
          res.projects.forEach(function (project) {
            projectsArr[project._id] = project.name
          });

          var categoriesArr = {
            null: ''
          };
          res.categories.forEach(function (categorie) {
            categoriesArr[categorie._id] = categorie.name
          });

          $scope.shifts = res.shifts.map(function (x) {
              x.userName = usersArr[x.user];
              x.categoryName = categoriesArr[x.category];
              x.projectsName = categoriesArr[x.project];
              x.buttons = {
                edit: x.status !== 'approved' && x.status !== 'rejected' && x.status !== 'pending',
                approved: x.status === 'approved' || x.status === 'pending',
                rejected: x.status === 'rejected' || x.status === 'pending',
                blocked: x.status === 'blocked'
              };

              return x;

            });

          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');

        })
      });

    };

    var lastId = null;
    $scope.showDivider = function (id) {
      var bool = (id !== null && lastId !== id);
      lastId = id;
      return bool;
    };

    $scope.next = function () {
      date = date.add(1, 'days');
      $scope.init();
    };

    $scope.back = function () {
      date = date.add(-1, 'days');
      $scope.init();
    };

    $scope.approve = function (shift) {
      if ($scope.hasRole('owner') || $scope.hasRole('approver')) {
        $http.post("/api/shifts/" + shift._id + "/approve")
          .success(function (data) {
            $scope.init();
          });
      }
    };

    $scope.reject = function (shift) {

      if ($scope.hasRole('owner') || $scope.hasRole('approver')) {
        $http.post("/api/shifts/" + shift._id + "/reject")
          .success(function (data) {
            $scope.init();
          });
      }
    };


    $scope.approveAll = function () {
      for (var shifts in $scope.timesheets) {
        for (var i = 0; i < $scope.timesheets[shifts].length; i++) {
          if ($scope.timesheets[shifts][i].shift.status == 'pending') {
            $scope.approve($scope.timesheets[shifts][i].shift)
          }
          ;
        }
      }
    };


  })
