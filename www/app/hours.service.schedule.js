angular.module('hours.service.schedule', [])
  .factory('scheduleService', function ($http, organisationService) {
    return {

      findAllShifts: function (organisationId) {
        return $http.get("/api/organisations/" + organisationId + "/shifts").then(function (shift) {
          return shift.data;
        });
      },

      findByDate: function (organisationId, date) {
        return $http.get("/api/organisations/" + organisationId + "/shifts?date=" + date).then(function (shift) {
          return shift.data;
        });
      }
    }

  });



