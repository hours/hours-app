angular.module('hours.service.reports', [])

  .factory('reportsService', function ($http, organisationService) {

    return {

      findAll: function (organisationId) {

        return $http.get("/api/organisations/" + organisationId + "/reports").then(function (res) {
          return res.data
        });

      }

    }

  });
