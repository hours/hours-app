angular.module('hours.service.categories', [])

  .factory('categoryService', function ($http) {

    return {
      findAll: function (organisationId) {
        return $http.get("/api/organisations/" + organisationId + '/categories').then(function (res) {
          return res.data;
        });
      }

    }
  });
