angular.module('hours.login.controllers', [
    'ionic'
  ])

  .controller('LoginController', function ($q, $scope, $state, $stateParams, $window, $ionicPopup, $ionicHistory, $ionicLoading, cacheService, tokenService, storageService, barcodeService, LOGIN_URL) {

    var ref = null;

    var storeToken = function (event) {

      if (event && event.url && typeof event.url === 'string') {

        var url = event.url;

        var access_token = url.match(/access_token=(.+)/);

        if (access_token) {
          storageService.setToken(access_token[1]).then(function () {
            ref.close();

            $ionicLoading.show({
              template: 'Loading...'
            });

            $q.all({
              user: cacheService.user(),
              organisation: cacheService.organisation()
            }).then(function () {
              cacheService.flush();
              $ionicHistory.clearCache();
              $ionicLoading.hide();
              $state.go('index');
            },function () {
              cacheService.flush();
              $ionicHistory.clearCache();
              $ionicLoading.hide();
              $state.go('setup');
            });
          })
        }
      }
    };

    $scope.loginLocal = function () {
      if (window.cordova) {
        ref = cordova.InAppBrowser.open(LOGIN_URL + '/auth/local', '_blank', 'location=yes');
        ref.addEventListener("loadstart", storeToken);
      } else {
        window.location.href = LOGIN_URL + "?redirect=local-app";
      }
    };

    $scope.loginFacebook = function () {
      if (window.cordova) {
        ref = cordova.InAppBrowser.open(LOGIN_URL + '/auth/facebook', '_blank', 'location=yes');
        ref.addEventListener("loadstart", storeToken);
      } else {
        window.location.href = LOGIN_URL + "?redirect=local-app";
      }

    };

    $scope.loginQrcode = function () {
      if (window.cordova) {
        barcodeService.scanQrcode().then(function (res) {

          var data = tokenService.decode(res.token);

          if(!data.token){
            $ionicPopup.alert({
              title: 'QR code',
              template: 'QRcode cannot be used to login.'
            });
            return;
          }

          if(res.action === 'login'){
            cacheService.flush();
            $ionicHistory.clearCache();
            $state.go('token-login', {
              token: res.token
            });
          }
        }, function (err) {
          $ionicPopup.alert({
            title: 'QR code',
            template: 'Error in reading QRcode.'
          });
        });
      } else {
        window.location.href = LOGIN_URL + "?redirect=local-app";
      }
    };

    $scope.createAccount = function () {
      $state.go("subscribe")
    };

    $scope.logout = function () {
      storageService.clean();
      cacheService.flush();
      $ionicHistory.clearCache();
      $state.go("login");
    };

    $scope.getToken = function () {
      return localStorage.getItem("access_token");
    }

  })

  .controller('SubscribeController', function ($scope, $http, $state, $ionicLoading, $timeout) {

    //var organisationId = $stateParams.organisationId;

    $scope.register = function (value, user) {
      if (user.password != user.repassword) {
        showMessage("Passwords do not match.");
        return false;
      }

      else if (value) {
        $http.post("/api/subscribe", user)
          .success(function () {
            showMessage("Account created, please login.");
            $state.go("login");
          })
          .error(function (data) {
            showMessage("User already exists.");
          });
      }
    };

    var showMessage = function (message) {
      $ionicLoading.show({
        template: message
      });

      $timeout(function () {
        $ionicLoading.hide();
      }, 2000);
    }
  })
