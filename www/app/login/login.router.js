angular.module('hours.login.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

    // Each tab has its own nav history stack:
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController'
      })

      .state('subscribe', {
        url: '/subscribe',
        templateUrl: 'app/login/login-subscribe.html',
        controller: 'SubscribeController'

      })
  });
