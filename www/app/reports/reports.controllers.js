angular.module('hours.reports.controllers', [])

  .controller('ReportsController', function ($q, $scope, $log, $http, $stateParams, $state, $ionicLoading, $ionicPopup, $timeout, cacheService, reportsService) {

    var organisationId = null;

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });


    $scope.init = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });
      cacheService.organisation().then(function (organisation) {
        $scope.organisation = organisation
        reportsService.findAll(organisation._id).then(function (data) {
          $scope.reports = data;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        }, function(){
          $scope.reports = null;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
      });
    };

    $scope.generate = function () {
      var popup = openPopup();

      popup.then(function (report) {

        if (report) {

          $http.post("/api/reports", report).then(
            function (res) {
              $scope.init();
            },
            function (res) {
              var data = res.data;
              var errorMessage = null;
              if (data === "NO_USERS_IN_REPORT")
                errorMessage = "No hours have been submitted this month."
              if (data === "INSUFFICIENT_BALANCE")
                errorMessage = "Not enough credits to generate report."
              if (data === "USER_NOT_ALLOWED_TO_GENERATE_REPORT")
                errorMessage = "Only the owner of the organisation can generate a report."
              if (errorMessage) {
                $ionicLoading.show({
                  template: errorMessage
                });
                $timeout(function () {
                  $ionicLoading.hide();
                }, 3000);
              }
            });
        }
      });
    };

    $scope.shareCsv = function (report) {
      $http.get("/api/reports/" + report._id + "/csv")
        .success(function (data) {
          var message = "hours";
          var subject = report.name + "-" + report.description;
          var file = "data:text/csv;base64," + data
          window.plugins.socialsharing.shareViaEmail(message, subject, null, null, null, file);
        });
    };

    $scope.shareXlsx = function (report) {
      $http.get("/api/reports/" + report._id + "/xlsx")
        .success(function (data) {
          var message = "hours";
          var subject = report.name + " - " + report.description;
          var file = "data:text/xlsx;base64," + data
          window.plugins.socialsharing.shareViaEmail(message, subject, null, null, null, file);
        });
    };

    $scope.goDetail = function (report) {
      $state.go("tab.reports-detail", {"reportId": report._id})
    };


    var openPopup = function (organisation) {
      $scope.report = {};

      $http.get("/api/organisations", {
          params: {
            "role": "owner"
          }
        })
        .success(function (data) {
          $scope.organisations = data;
        });

      return $ionicPopup.show({
        templateUrl: 'app/reports/reports-generate.html',
        title: 'Generate Report',
        scope: $scope.$new(),
        buttons: [
          {
            text: 'Cancel',
          },
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (this.scope.reportOrg.$valid) {
                var report = $scope.report;
                report.organisation = $scope.organisation._id;
                return report
              } else {
                e.preventDefault();
              }
            }
          }
        ]
      });
    }
  })

  .controller('ReportsDetailController', function ($scope, $log, $http, $stateParams, $state, $ionicLoading, $ionicPopup) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });

    var reportId = $stateParams.reportId;


    $scope.init = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $scope.list = {};

      $http.get("/api/reports/" + reportId)
        .success(function (report) {

          var findUserById = function (userId) {

            var result = null;
            angular.forEach(report.users, function (user) {
              if (user._id === userId) {
                result = user;
              }
            });
            return result;
          }


          angular.forEach(report.shifts, function (shift) {
            if (!$scope.list[shift.user]) $scope.list[shift.user] = {
              "total": 0,
              "user": findUserById(shift.user)
            };

            $scope.list[shift.user].total += shift.total;
          });

          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');


        })
        .error(function () {
          $ionicLoading.hide();
        });

    }

  })
