angular.module('hours.reports.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('tab.reports', {
        url: '/reports',
        views: {
          'tab-reports': {
            templateUrl: 'app/reports/reports.html',
            controller: 'ReportsController'
          }
        }
      })

      .state('tab.reports-detail', {
        url: '/detail/:reportId',
        views: {
          'tab-reports': {
            templateUrl: 'app/reports/reports-detail.html',
            controller: 'ReportsDetailController'
          }
        }
      })
  });
