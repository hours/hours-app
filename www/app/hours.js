angular.module('hours', [

  'hours.router',
  'hours.config',
  'hours.interceptor',

  'hours.service.reports',
  'hours.service.organisations',
  'hours.service.users',
  'hours.service.employees',
  'hours.service.storage',
  'hours.service.camera',
  'hours.service.barcode',
  'hours.service.devices',
  'hours.service.token',
  'hours.service.schedule',
  'hours.service.analytics',
  'hours.service.cache',
  'hours.service.projects',
  'hours.service.categories',

  'hours.controller.tab',

  'hours.sheets',
  'hours.reports',
  'hours.login',
  'hours.settings',
  'hours.employees',
  'hours.setup',
  'hours.schedule',
  'hours.token'

]);
