angular.module('hours.service.credits', [])

  .factory("creditsService", function ($q, $http, cacheService) {

    return {
      buyCredits: function (credits, amount) {

        return $q(function (resolve, reject) {
          var payment = {
            amount: amount,
            credits: credits
          };

          cacheService.organisation().then(function (organisation) {

            $http.post("/api/organisations/" + organisation._id + "/transactions/", payment)
              .success(function (data) {
                if (window.cordova && window.cordova.InAppBrowser) {
                  var ref = cordova.InAppBrowser.open(data.links.paymentUrl, '_blank', 'location=yes');
                  var paymentCheck = function (event) {
                    var url = event.url;
                    var payment_ok = url.match("http://hours-app.com/");
                    if (payment_ok) {
                      ref.close();
                      resolve();
                    }
                  };
                  ref.addEventListener("loadstart", paymentCheck);
                } else {
                  window.open(data.links.paymentUrl, '_blank');
                  resolve();
                }
              });
          });
        });
      }
    }
  });
