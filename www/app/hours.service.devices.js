angular.module('hours.service.devices', [])

  .factory('devicesService', function ($http, $ionicPlatform) {

    return {

      save: function () {

        $ionicPlatform.ready(function () {

          var device = window.device || {
              uuid: '0000-0000-0000-0000'
            };

          return $http.post("/api/devices", device).then(function (response) {
            return response.data;
          }, function () {
            return null;
          });

        })
      }
    }
  });
