angular.module('hours.setup.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('setup', {
        url: '/setup',
        templateUrl: 'app/setup/setup.html',
        controller: 'SetupController'
      })

      .state('setup-join', {
        url: '/setup/join',
        templateUrl: 'app/setup/setup-join.html',
        controller: 'SetupJoinController'
      })

      .state('setup-organisation', {
        url: '/setup/organisation',
        templateUrl: 'app/setup/setup-organisation.html',
        controller: 'SetupOrganisationController'
      })

      .state('setup-share', {
        url: '/setup/share',
        templateUrl: 'app/setup/setup-share.html',
        controller: 'SetupShareController'
      })

      .state('setup-credits', {
        url: '/setup/credits',
        templateUrl: 'app/setup/setup-credits.html',
        controller: 'SetupCreditsController'
      })

  });
