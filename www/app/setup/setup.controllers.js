angular.module('hours.setup.controllers', [
    'ionic'
  ])

  .controller('SetupController', function ($scope, $state) {

    $scope.test = 'TEST';

    $scope.goSkip = function () {
      $state.go('index')
    };

    $scope.goJoin = function () {
      $state.go('setup-join')
    };

    $scope.goCreate = function () {
      $state.go('setup-organisation')
    };

  })

  .controller('SetupJoinController', function ($scope, $state, barcodeService) {
    $scope.goSkip = function () {
      $state.go('index')
    };

    $scope.scan = function () {
      barcodeService.scanQrcode().then(function (res) {
        if(res.action === 'organisation'){
          $state.go('token-organisation', {
            token: res.token
          });
        }
      });
    }
  })

  .controller('SetupOrganisationController', function ($scope, $state, organisationService) {

    $scope.organisation = {};

    $scope.save = function (organinsation) {

      if (organinsation && organinsation.name != "") {


        organisationService.save(organinsation).then(function (organisations) {
          $state.go('setup-share')
        });
      }
    }

    $scope.goSkip = function () {
      $state.go('index')
    };
  })

  .controller('SetupShareController', function ($scope, $state, organisationService, tokenService, cacheService) {
    cacheService.organisation().then(function (organisation) {
      $scope.url = "http://hours-app.com/token.html#/organisation/" + tokenService.encode(organisation);
      $scope.qrcode = "https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=" + encodeURIComponent($scope.url) + "&choe=UTF-8&chld=L|0";
      $scope.organisation = organisation;
    });

    $scope.send = function () {
      if (window.plugins && window.plugins.socialsharing) {
        var message = "Klik op deze link om de organisatie " + $scope.organisation.name + " toe te voegen aan de Hours app";
        var subject = $scope.organisation.name;
        window.plugins.socialsharing.share(message, subject, null, $scope.url);
      } else {
        alert('share: ' + $scope.url);
      }
    }

    $scope.goNext = function () {
      $state.go('setup-credits')
    };

    $scope.goSkip = function () {
      $state.go('index')
    };
  })

  .controller('SetupCreditsController', function ($scope, $state, $ionicPopup, creditsService) {

    $scope.goStart = function () {
      $state.go('index')
    };

    $scope.buy = function () {
      var scope = $scope.$new();

      scope.buyCredits = function (credits, amount) {
        creditsService.buyCredits(credits, amount).then(function(){
          popup.close();
          $scope.success = true;
          $scope.credits = credits;
          $scope.amount = amount;
        });
      };

      var popup =  $ionicPopup.show({
        templateUrl: 'app/settings/settings-transactions-form.html',
        title: 'Credits',
        scope: scope,
        buttons: [
          {text: 'Cancel'}
        ]
      });
    };

    $scope.goSkip = function () {
      $state.go('index')
    };
  });
