angular.module('hours.config', [])

  // http
  .config(function ($httpProvider) {
    var tz = jstz.determine();
    $httpProvider.defaults.headers.common['X-TimeZone'] = tz.name();
    $httpProvider.defaults.timeout = 5000;
    $httpProvider.interceptors.push('ApiInterceptor');
    $httpProvider.interceptors.push('AuthInterceptor');
  })

  //Login
  .run(function ($rootScope, $http, $state, $location, $window, cacheService) {

    var hash = $window.location.hash
    if (hash.indexOf("#/access_token=") === 0) {
      var token = hash.replace("#/access_token=", "");
      localStorage.setItem("access_token", token);
      cacheService.organisation().then(function () {
        $state.go('index');
      }, function () {
        console.log('Go to SETUP');
        $state.go('setup');
      })

    }

  })

  // register device
  .run(function ($ionicPlatform, devicesService, storageService) {
    storageService.getToken().then(function (token) {
      if (!token)
        return;
      devicesService.save();
    })

  })

  // analytics
  .run(function ($rootScope, $ionicPlatform, analyticsService) {

    $ionicPlatform.ready(function () {
      if (window.ga && window.ga.startTrackerWithId && window.ga.setAppVersion) {
        window.ga.startTrackerWithId('UA-85009150-1')
        window.ga.setAppVersion(APP_VERSION)
      }
    });

    $rootScope.$on("$ionicView.enter", function (event, data) {
      analyticsService.trackView(data.stateName);
    });

    $rootScope.$on("hours.user.change", function (event, data) {
      analyticsService.setUserId(data._id);
    });

  })

  // init
  .run(function ($q, $rootScope, $state, storageService, cacheService) {

    storageService.getToken().then(function (token) {
      if (token) {
        $q.all({
          user: cacheService.user(),
          organisation: cacheService.organisation()
        }).then(function (res) {
          $rootScope.user = res.user;
          $rootScope.organisation = res.organisation;
          $state.go('index');
        }, function () {
          $rootScope.user = null;
          $rootScope.organisation = null;
          $state.go('login');
        });

      } else {
        $state.go('login');
      }
    });

    $rootScope.hasRole = function (role) {
      return ($rootScope.organisation && $rootScope.organisation.role === role)
    };

    $rootScope.hasFeature = function (feature) {
      return (
        $rootScope.organisation &&
        $rootScope.organisation.features &&
        $rootScope.organisation.features.app &&
        $rootScope.organisation.features.app.indexOf &&
        $rootScope.organisation.features.app.indexOf(feature) >= 0
      )
    };

  })
;
