angular.module('hours.service.employees', [])

  .factory('employeesService', function ($http) {

    return {

      findAll: function (organisationId) {
        return $http.get("/api/organisations/" + organisationId + "/users").then(function (res) {
          return res.data
        });
      },

      find: function (organisationId, employeeId) {
        if (employeeId === 'me') {
          return $http.get("/api/users/me").then(function (response) {
            return response.data;
          }, function (res) {
            return null;
          });
        } else {
          return $http.get("/api/organisations/" + organisationId + "/users/" + employeeId).then(function (response) {
            return response.data;
          }, function (res) {
            return null;
          });
        }
      },

      count: function (organisationId, employeeId) {
        return $http.get("/api/organisations/" + organisationId + "/users/" + employeeId + "/count").then(function (res) {
          return res.data
        });
      }
    };

  });
