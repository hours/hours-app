angular.module('hours.service.projects', [])

  .factory('projectService', function ($http) {

    return {

      findAll: function (organisationId) {
        return $http.get("/api/organisations/" + organisationId + '/projects').then(function (res) {
          return res.data;
        });
      }

    }
  });
