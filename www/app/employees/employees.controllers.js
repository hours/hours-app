angular.module('hours.employees.controllers', [
    'ionic'
  ])

  .controller('EmployeesController', function ($q, $scope, $http, $state, $ionicLoading, cacheService, employeesService, $ionicPopup) {

    var organisationId = null;
    $scope.organisationId=organisationId;

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });


    $scope.init = function () {

      $scope.count = {};
      $scope.employees = null;

      $ionicLoading.show({
        template: 'Loading...',
        scope: $scope
      });

      $q.all({
        organisation: cacheService.organisation(),
        employees: cacheService.employees()
      }).then(function (res) {

        $scope.organisationId=res.organisation._id;
        $scope.employees = res.employees;

        // Count pending shifts
        $scope.employees.forEach(function (employee) {
          var employeeId = employee._id;

          var url = "/api/organisations/" + res.organisation._id + "/users/" + employeeId + "/shifts/count?status=pending";
          $http.get(url).then(function (res) {
            employee.count = res.data;
          });
        });

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');

      });
    };

    $scope.select = function (employee) {
      $state.go('tab.sheets', {
        employeeId: employee._id
      });
    };


    $scope.remove = function (employee) {
    console.log($scope.organisationId);
      var confirmPopup = $ionicPopup.confirm({
        title: 'Remove employee',
        template: 'Are you sure you want to remove this employee?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          $http.delete("/api/organisations/" + $scope.organisationId + "/users/" + employee._id)
            .success(function (data) {
              cacheService.flush();
              $scope.init();
            });
        }
      });
    };

    $scope.edit = function (employee) {
      var popup = openPopup(employee);
      popup.then(function (employee) {
        if (employee) {
          $http.put("/api/organisations/" + $scope.organisationId + "/users/" + employee._id, employee)
            .success(function (data) {
              cacheService.flush();
              $scope.init();
            });
        }
        else {
          cacheService.flush();
          $scope.init();
        }
      });
    };

    var openPopup = function (employee) {
      if (employee) $scope.employee = employee;
      else $scope.employee = {};
      return $ionicPopup.show({
        templateUrl: 'templates/employees-form.html',
        title: employee.name,
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              return $scope.employee;
            }
          }
        ]
      });
    }


  });
