angular.module('hours.employees.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('tab.employees', {
        url: '/employees',
        views: {
          'tab-sheets': {
            templateUrl: 'app/employees/employees.html',
            controller: 'EmployeesController'
          }
        }
      })


  });
