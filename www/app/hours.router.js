angular.module('hours.router', [
    'ionic'
  ])


  .config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider

      .state('tab', {
        url: '/tab',
        abstract: true,
        controller: 'TabCtrl',
        templateUrl: 'app/hours.tabs.html'
      })

      .state('index', {
        url: '/index',
        template: '<ion-view></ion-view>',
        controller: function ($scope, $state, $ionicHistory, cacheService) {
          $scope.$on('$ionicView.enter', function () {

            $ionicHistory.clearCache();

            cacheService.organisation().then(function (data) {
              if (!data) {
                $state.go('setup');
              }
              if (data.role === 'owner' || data.role === 'approver') {
                $state.go('tab.employees');
              } else {
                cacheService.user().then(function (data) {
                  $state.go('tab.sheets', {
                    employeeId: data._id
                  });
                });
              }
            });

          });
        }
      })
  });
