angular.module('hours.interceptor', [])

  .factory('ApiInterceptor', function ($log, $q, API_URL) {
    return {
      request: function (config) {

        var patt = /\/api\//g;
        if (patt.test(config.url)) {
          config.url = API_URL + config.url;
        }

        return config;
      }
    };
  })

  .factory('AuthInterceptor', function () {
    return {
      request: function (config) {
        if (localStorage.getItem("access_token") && !config.headers['Authorization']) {
          var token = localStorage.getItem("access_token");
          config.headers['Authorization'] = 'Bearer ' + token;
        }
        return config;
      }
    };
  });
