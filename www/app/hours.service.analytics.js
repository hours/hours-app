angular.module('hours.service.analytics', [])

  .factory("analyticsService", function ($q) {

    return {
      trackView: function (data) {
        return $q(function (resolve, reject) {
          if (window.ga && window.ga.trackView) {
            document.addEventListener("deviceready", function () {
              window.ga.trackView(data);
              resolve();
            }, false);
          } else {
            console.log("analytics trackView: ", data);
          }
        });
      },
      trackEvent: function (category, action, label, value) {
        return $q(function (resolve, reject) {
          if (window.ga && window.ga.trackEvent) {
            document.addEventListener("deviceready", function () {
              window.ga.trackEvent(category, action, label, value);
              resolve();
            }, false);
          } else {
            console.log("analytics trackEvent: ", category, action, label, value);
          }
        });
      },
      trackMetric: function (key, value) {
        return $q(function (resolve, reject) {
          if (window.ga && window.ga.trackMetric) {
            document.addEventListener("deviceready", function () {
              window.ga.trackMetric(key, value);
              resolve();
            }, false);
          } else {
            console.log("analytics trackMetric: ", key, value);
          }
        });
      },
      setUserId: function (id) {
        return $q(function (resolve, reject) {
          if (window.ga && window.ga.setUserId) {
            document.addEventListener("deviceready", function () {
              window.ga.setUserId(id);
              resolve()
            }, false);
          } else {
            console.log("analytics setUserId: ", id);
          }
        });
      }
    }

  });
