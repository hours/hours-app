angular.module('hours.service.cache', [
    'hours.service.users',
    'hours.service.organisations',
    'hours.service.projects',
    'hours.service.categories'
  ])

  .factory("cacheService", function ($q, $rootScope, storageService, userService, organisationService, projectService, categoryService, employeesService) {

    var data = {
      user: null,
      organisation: null,
      projects: null,
      categories: null,
      employees: null
    };

    return {

      flush: function () {
        data = {
          user: null,
          organisation: null,
          projects: null,
          categories: null,
          employees: null
        };
      },

      user: function () {
        return $q(function (resolve, reject) {

          if (data.user)
            return resolve(data.user);

          return userService.me().then(function (user) {
            data.user = user;
            $rootScope.user = user;
            resolve(data.user);
          }, reject);

        });

      },

      organisation: function () {

        return $q(function (resolve, reject) {

          if (data.organisation)
            return resolve(data.organisation);

          storageService.getOrganisation().then(function (id) {
            if(id){
              organisationService.find(id).then(function (organisation) {
                data.organisation = organisation;
                $rootScope.organisation = organisation;
                storageService.setOrganisation(organisation._id);
                resolve(data.organisation);
              }, reject)
            }else{
              organisationService.findAll().then(function (organisations) {
                if (organisations && organisations.length > 0) {
                  data.organisation = organisations[0];
                  $rootScope.organisation = organisations[0];
                  storageService.setOrganisation(organisations[0]._id);
                  resolve(data.organisation);
                } else {
                  reject();
                }
              }, reject)
            }
          },reject)

        });

      },

      projects: function () {

        return $q(function (resolve, reject) {

          if (data.projects)
            return resolve(data.projects);

          storageService.getOrganisation().then(function (id) {

            projectService.findAll(id).then(function (projects) {
              data.projects = projects;
              resolve(data.projects);
            },reject);

          });

        });
      },

      categories: function () {

        return $q(function (resolve, reject) {

          if (data.categories)
            return resolve(data.categories);

          storageService.getOrganisation().then(function (id) {

            categoryService.findAll(id).then(function (categories) {
              data.categories = categories;
              resolve(data.categories);
            },reject);

          });

        });
      },

      employees: function () {

        return $q(function (resolve, reject) {

          if (data.employees)
            return resolve(data.employees);

          storageService.getOrganisation().then(function (id) {

            employeesService.findAll(id).then(function (employees) {
              data.employees = employees;
              resolve(data.employees);
            },reject);

          });

        });
      }
    }

  });
