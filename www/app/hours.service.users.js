angular.module('hours.service.users', [])

  .factory('userService', function ($q, $http) {

    return {

      me: function () {
        return $http.get("/api/users/me").then(function (res) {
          return res.data;
        });
      }

    }
  });
