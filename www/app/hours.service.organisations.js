angular.module('hours.service.organisations', [])

  .factory('organisationService', function ($q, $http, $rootScope, storageService) {

    return {

      find: function (id) {
        return $http.get("/api/organisations/" + id).then(function (res) {
          return res.data
        })
      },

      findAll: function () {
        return $http.get("/api/organisations").then(function (res) {
            return res.data
          })
      },

      save: function (organinsation) {
        if (organinsation._id)
          return $http.put("/api/organisations/" + organinsation._id, organinsation);
        else

          return $http.post("/api/organisations/", organinsation).then(function (res) {
            storageService.setOrganisation(res.data._id);
            return res.data
          });
      },

      remove: function (organinsation) {
        return $http.delete("/api/organisations/" + organinsation._id);
      },

      join: function (id) {
        return $http.post("/api/organisations/" + id + "/join");
      }

    }

  });
