angular.module('hours.token.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('token-organisation', {
        url: '/token/organisation/:token',
        templateUrl: 'app/token/token-organisation.html',
        controller: 'OrganisationTokenController'
      })

      .state('token-login', {
        url: '/token/login/:token',
        templateUrl: 'app/token/token-login.html',
        controller: 'LoginTokenController'
      })

  });
