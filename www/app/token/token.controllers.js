angular.module('hours.token.controllers', [
    'ionic'
  ])

  .controller('OrganisationTokenController', function ($scope, $state, $stateParams, $ionicPopup, $ionicNavBarDelegate, tokenService, organisationService) {

    var token = $stateParams.token;
    var organisation = tokenService.decode(token);

    $scope.id = organisation.id;
    $scope.name = organisation.name;

    $scope.join = function () {
      organisationService.join(organisation.id).then(function () {
        $state.go('index');
      }, function (res) {
        if (res.data === 'ORGANISATION_NOT_FOUND') {
          $ionicPopup.alert({
            title: 'Cannot join organisation',
            template: 'Organisation not found.'
          });
        }
      });
    };

    $scope.cancel = function () {
      $ionicNavBarDelegate.back();
    };

  })

  .controller('LoginTokenController', function ($q, $http, $scope, $state, $stateParams, $ionicPopup, $ionicNavBarDelegate, tokenService, storageService, userService, organisationService, cacheService) {

    var token = $stateParams.token;
    var data = tokenService.decode(token);

    var config = {
      headers: {
        Authorization: 'Bearer ' + data.token,
        'Cache-Control': 'no-cache, no-store, must-revalidate',
        Pragma: 'no-cache',
        Expires: 0
      }
    };

    $q.all({
      user: $http.get('/api/users/me', config),
      organisation: $http.get('/api/organisations/' + data.organisation, config)
    }).then(function (res) {
      $scope.user = res.user.data
      $scope.organisation = res.organisation.data
    });

    $scope.login = function () {
      $q.all([
        storageService.setToken(data.token),
        storageService.setOrganisation(data.organisation)
      ]).then(function () {
        $q.all([
          cacheService.flush()
        ]).then(function () {
          $state.go('index');
        });
      });
    };

    $scope.cancel = function () {
      $ionicNavBarDelegate.back();
    };

  });
;
