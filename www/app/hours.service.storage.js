angular.module('hours.service.storage', [])

  .factory("storageService", function ($q) {

    return {

      getOrganisation: function () {
        return $q(function (resolve, reject) {
          var organisationId = localStorage.getItem('organisation')
          resolve(organisationId);
        });
      },

      setOrganisation: function (organisationId) {
        return $q(function (resolve, reject) {
          if(organisationId) {
            localStorage.setItem('organisation', organisationId);
            resolve(organisationId);
          }else{
            reject();
          }
        });
      },

      removeOrganisation: function () {
        return $q(function (resolve, reject) {
          resolve(localStorage.removeItem('organisation'));
        });
      },

      getToken: function(){
        return $q(function (resolve, reject) {
          resolve(localStorage.getItem('access_token'));
        });
      },

      setToken: function(token){
        return $q(function (resolve, reject) {
          localStorage.setItem('access_token', token);
          localStorage.removeItem('organisation');
          resolve(localStorage.getItem('access_token'));
        });
      },

      clean: function(){
        return $q(function (resolve, reject) {
          localStorage.removeItem('access_token');
          localStorage.removeItem('organisation');
          resolve();
        });
      }
    }

  });
