angular.module('hours.sheets.controllers', [
    'ionic'
  ])

  .controller('SheetsController', function ($q, $log, $scope, $rootScope, $filter, $timeout, $http, $state, $stateParams, $ionicPopup, $ionicModal, $ionicLoading, $ionicTabsDelegate, employeesService, cacheService, sheetService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
      console.log($scope)
    });

    var employeeId = $stateParams.employeeId;
    var weekId = $stateParams.weekId;


    if (!weekId)
      weekId = moment.utc().format("YYYYWW");
      console.log('weekid' + moment.utc().format('ww'));

    $scope.me = function () {
      return ($scope.user && employeeId === $scope.user._id);
    };

    $scope.date = moment().year(weekId.substr(0, 4)).isoWeek(weekId.substr(4, 6)).startOf('isoWeek');
    //console.log('scope'+ moment().year(weekId.substr(0, 4)).week(weekId.substr(4, 6)).startOf('week'));

    $scope.init = function () {

      $ionicLoading.show({
        template: 'Loading...'
      });

      $q.all({
        user: cacheService.user(),
        organisation: cacheService.organisation(),
        projects: cacheService.projects(),
        categories: cacheService.categories()
      }).then(function (res) {

        $scope.projects = {};
        for (var i = 0; i < res.projects.length; i++) {
          var project = res.projects[i]
          $scope.projects[project._id] = project;
        }

        $scope.categories = {};
        for (var i = 0; i < res.categories.length; i++) {
          var category = res.categories[i]
          $scope.categories[category._id] = category;
        }

        $q.all({
          employee: employeesService.find(res.organisation._id, employeeId),
          data: sheetService.find(res.organisation._id, employeeId, weekId)
        }).then(function (res) {

          $scope.employee = res.employee;

          $scope.total = res.data.total;
          $scope.timesheets = res.data.timesheets;

          // Set details
          for (date in $scope.timesheets) {
            var timesheets = $scope.timesheets[date];
            timesheets.map(function (timesheet) {
              timesheet.buttons = {
                edit: $scope.me() && (timesheet.shift.status === 'planned' || timesheet.shift.status === 'pending'),
                rejected: timesheet.shift.status === 'rejected' || (!$scope.me() && timesheet.shift.status === 'pending'),
                approved: timesheet.shift.status === 'approved' || (!$scope.me() && timesheet.shift.status === 'pending'),
                blocked:  timesheet.shift.status === 'blocked'
              };
              return timesheet;

            });
          }
          $scope.$broadcast('scroll.refreshComplete');
          $ionicLoading.hide();
        }, function () {
          $scope.$broadcast('scroll.refreshComplete');
          $ionicLoading.hide();
        });


      }, function () {
        $scope.$broadcast('scroll.refreshComplete');
        $ionicLoading.hide();
      });

    };

    $scope.moment = function (date) {
      return moment(date)
    };

    $scope.check = function (shift) {

      var data = {
        from: new Date(shift.from),
        to: new Date(shift.to),
        pause: shift.pause / 60000
      };

      $ionicPopup.alert({
        title: 'Worked hours',
        subTitle: $filter('date')(data.from, "EEE, d MMM"),
        templateUrl: 'templates/timesheet-check.html',
        scope: popupScope
      });

    };

    $scope.crew = function (shift) {
      var scope = $scope.$new();
      scope.status = shift.status
      $http.get('/api/shifts/' + shift._id + '/crew').success(function (crew) {
        scope.crew = crew.map(function (x) {
          x.from = moment(x.shift.from).format('HH:mm');
          x.to = moment(x.shift.to).format('HH:mm');
          return x;
        });
        $ionicPopup.show({
          templateUrl: 'app/sheets/sheets-popup-crew.html',
          title: 'Crew',
          subTitle: 'Crew information',
          scope: scope,
          buttons: [
            {text: 'Close'}
          ]
        });
      });
    };

    $scope.details = function (shift) {
      var scope = $scope.$new();
      scope.category = $scope.getCategoryName(shift.shift.category);
      scope.project = $scope.getProjectName(shift.shift.project);
      scope.shift = shift.shift;
      scope.from = moment(scope.shift.from).format('HH:mm');
      scope.to = moment(scope.shift.to).format('HH:mm');
      scope.picture = scope.shift.picture;
      $ionicPopup.show({
        templateUrl: 'app/sheets/sheets-popup-details.html',
        title: 'Info',
        subTitle: 'Shift information',
        scope: scope,
        buttons: [
          {text: 'Close'}
        ]

      });
    };

    $scope.approve = function (shift) {

      if ($scope.me() && ($scope.hasRole('owner')))
        return $scope.edit(shift);

      if ($scope.hasRole('owner') || $scope.hasRole('approver'))
        $http.post("/api/shifts/" + shift._id + "/approve")
          .success(function (data) {
            $scope.init();
          });
    };

    $scope.approveAll = function () {
      for (var shifts in $scope.timesheets) {
        for (var i = 0; i < $scope.timesheets[shifts].length; i++) {
          if ($scope.timesheets[shifts][i].shift.status == 'pending') {
            $scope.approve($scope.timesheets[shifts][i].shift)
          }
        }
      }
    };

    $scope.reject = function (shift) {

      if ($scope.me() && ($scope.hasRole('owner') || $scope.hasRole('approver')))
        return $scope.edit(shift);

      if ($scope.me() && $scope.hasRole('worker'))
        return $scope.edit(shift);

      if ($scope.hasRole('owner') || $scope.hasRole('approver'))
        $http.post("/api/shifts/" + shift._id + "/reject")
          .success(function (data) {
            $scope.init();
          });
    };

    $scope.next = function (timesheet) {
      weekId = $scope.date.add(1, 'week').format("YYYYWW");
      if (weekId== '201801' && $scope.date.format('YYYY-MM-DD')=='2018-12-31') weekId='201901'
      $scope.init();
    };

    $scope.back = function (timesheet) {
      weekId=$scope.date.subtract(1, 'week').format("YYYYWW");
      if (weekId== '201801' && $scope.date.format('YYYY-MM-DD')=='2018-12-31') weekId='201901';
      $scope.init()
    };

    $scope.swap = function (shift) {
      var url = "http%3A%2F%2Fhours-app.com%2Ftoken.html%23%2Fswap%2F" + tokenService.encode($scope.organisation);
      var subject = "Swap shift " + $scope.organisation.name;
      var message = "Click on this link to assign to this shift";
      window.plugins.socialsharing.share(message, subject, null, url);
    };


    $scope.add = function (date) {

      $ionicModal.fromTemplateUrl('app/sheets/sheets-modal.html', function () {

        return {
          scope: $scope.$new(),
          animation: 'slide-in-up'
        }

      }).then(function (modal) {

        modal.scope.hasFeature = $scope.hasFeature;

        modal.scope.type = null;

        modal.scope.init = $scope.init;
        modal.scope.date = moment(date).toDate();
        modal.scope.organisation = $scope.organisation;
        modal.scope.projects = $scope.projects;
        modal.scope.categories = $scope.categories;

        modal.scope.hasProjects = function(){
          return Object.keys(modal.scope.projects).length > 0;
        };

        modal.scope.data = {
          from: moment(date).hours(12).toDate(),
          to: moment(date).hours(12).toDate(),
          organisation: $scope.organisation._id,
          status: null
        };

        modal.scope.cancel = function () {
          modal.remove();
        }

        modal.show();

      });

    };

    $scope.edit = function (shift) {

      $ionicModal.fromTemplateUrl('app/sheets/sheets-modal.html', function () {

        return {
          scope: $scope.$new(),
          animation: 'slide-in-up'
        }

      }).then(function (modal) {

        modal.scope.type = shift.type;
        modal.scope.init = $scope.init;
        modal.scope.hasFeature = $scope.hasFeature;

        modal.scope.type = shift.type;

        if (shift.type === 'SHIFT')
          $ionicTabsDelegate.$getByHandle('sheet')._instances[1].select(0);
        if (shift.type === 'BLOCKED')
          $ionicTabsDelegate.$getByHandle('sheet')._instances[1].select(1);
        if (shift.type === 'EXPENSE')
          $ionicTabsDelegate.$getByHandle('sheet')._instances[1].select(2);
        if (shift.type === 'MILEAGE')
          $ionicTabsDelegate.$getByHandle('sheet')._instances[1].select(3);


        modal.scope.date = shift.from || shift.date;
        modal.scope.organisation = $scope.organisation;
        modal.scope.projects = $scope.projects;
        modal.scope.categories = $scope.categories;

        modal.scope.hasProjects = function(){
          return Object.keys(modal.scope.projects).length > 0;
        };

        modal.scope.data = shift;

        modal.scope.data.from = new Date(shift.from);
        modal.scope.data.to = new Date(shift.to);
        modal.scope.data.date = new Date(shift.date);
        modal.scope.data.pause = shift.pause / 60000;

        modal.scope.picture = "data:image/gif;base64," + shift.picture;

        modal.scope.cancel = function () {
          modal.remove();
        };

        modal.show();

      });

    };

  })

  .controller('SheetShiftController', function ($scope, $http, $ionicLoading, $ionicPopup, sheetService, analyticsService) {

    $scope.save = function () {

     $scope.shiftForm.$submitted = true;

      if ($scope.shiftForm.$invalid)
        return;

      var data = {
        _id: $scope.data._id,
        type: 'SHIFT',
        organisation: $scope.organisation._id,
        from: $scope.data.from,
        to: $scope.data.to,
        pause: $scope.data.pause * 60000,
        project: $scope.data.project,
        category: $scope.data.category,
        user: $scope.data.user,
        status: $scope.data.status
      };

      console.log('shift');
      console.log(data);

      // total
      if (data.from.getTime() == data.to.getTime()) {
        $ionicPopup.alert({
          title: 'Alert',
          template: 'Start and End time are equal!'
        });
      } else {
        $ionicLoading.show({template: 'Loading...'});
        sheetService.save(data).then(function () {
          $scope.init();
          analyticsService.trackEvent('shift', 'save');
          $scope.modal.remove();
        }, function () {
          $ionicLoading.hide();
          $scope.modal.remove();
        })

      }

    };

    $scope.delete = function () {

      $ionicPopup.confirm({
        title: 'Delete',
        template: '<p>Are you sure you want to delete this shift?</p>'
      }).then(function (res) {

        if (res) {
          $ionicLoading.show({template: 'Loading...'});
          var id = $scope.modal.scope.data._id;
          sheetService.delete(id).then(function () {
            $scope.init();
            $scope.modal.remove();
          },function(){
            $ionicLoading.hide();
            $scope.modal.remove();
          });
        } else {
          $scope.modal.remove();
        }
      }, function () {
        $scope.modal.remove();
      })

    }
  })

  .controller('SheetExpenseController', function ($scope, $http, $ionicLoading, $ionicPopup, sheetService, cameraService) {

    $scope.getPicture = function () {
      cameraService.getPicture().then(function (picture) {
        $scope.picture = "data:image/gif;base64," + picture;
        $scope.data.picture = picture
      });
    };

    $scope.save = function () {

      $scope.expensesForm.$submitted = true;

      if ($scope.expensesForm.$invalid)
        return;

      $ionicLoading.show({template: 'Loading...'});
      var data = {
        _id: $scope.data._id,

        type: 'EXPENSE',

        organisation: $scope.data.organisation,
        user: $scope.data.user,

        date: $scope.date,
        description: $scope.data.description,
        amount: $scope.data.amount,
        picture: $scope.data.picture,

        project: $scope.data.project,
        category: $scope.data.category,

        status: $scope.data.status
      };
      sheetService.save(data).then(function () {
        $scope.init();
        $scope.modal.remove();
      }, function () {
        $ionicLoading.hide();
        $scope.modal.remove();
      })
    };

    $scope.delete = function () {

      $ionicPopup.confirm({
        title: 'Delete',
        template: '<p>Are you sure you want to delete this expense?</p>'
      }).then(function (res) {

        if (res) {
          $ionicLoading.show({template: 'Loading...'});
          var id = $scope.modal.scope.data._id;
          sheetService.delete(id).then(function () {
            $scope.init();
            $scope.modal.remove();
          });
        } else {
          $scope.modal.remove();
        }
      })

    }
  })

  .controller('SheetMileageController', function ($scope, $http, $ionicLoading, $ionicPopup, sheetService, cameraService) {

    $scope.save = function () {

      $scope.mileageForm.$submitted = true;

      if ($scope.mileageForm.$invalid)
        return;

      $ionicLoading.show({template: 'Loading...'});
      var data = {
        _id: $scope.data._id,

        type: 'MILEAGE',

        organisation: $scope.data.organisation,
        user: $scope.data.user,

        date: $scope.date,
        distance: $scope.data.distance,
        origin: $scope.data.origin,
        destination: $scope.data.destination,

        project: $scope.data.project,
        category: $scope.data.category,

        status: $scope.data.status
      };
      sheetService.save(data).then(function () {
        $scope.init()
        $scope.modal.remove();
      }, function () {
        $ionicLoading.hide();
        $scope.modal.remove();
      })
    };

    $scope.delete = function () {

      $ionicPopup.confirm({
        title: 'Delete',
        template: '<p>Are you sure you want to delete this mileage?</p>'
      }).then(function (res) {

        if (res) {
          $ionicLoading.show({template: 'Loading...'});
          var id = $scope.modal.scope.data._id;
          sheetService.delete(id).then(function () {
            $scope.init();
            $scope.modal.remove();
          });
        } else {
          $scope.modal.remove();
        }
      })

    }

  })

  .controller('SheetAvailabilityController', function ($scope, $http, $ionicLoading, $ionicPopup, sheetService, analyticsService) {

      $scope.save = function () {

        $scope.availabilityForm.$submitted = true;

        if ($scope.availabilityForm.$invalid)
          return;

        var data = {
          _id: $scope.data._id,
          type: 'BLOCKED',
          organisation: $scope.organisation._id,
          from: $scope.data.from,
          to: $scope.data.to,
          user: $scope.data.user,
          status: $scope.data.status
        };



        // total
        if (data.from.getTime() == data.to.getTime()) {
          $ionicPopup.alert({
            title: 'Alert',
            template: 'Start and End time are equal!'
          });
        } else {
          $ionicLoading.show({template: 'Loading...'});
          sheetService.save(data).then(function () {
          console.log(data);
            $scope.init();
            analyticsService.trackEvent('availability', 'save');
            $scope.modal.remove();
          }, function () {
            $ionicLoading.hide();
            $scope.modal.remove();
          })

        }

      };

      $scope.delete = function () {

        $ionicPopup.confirm({
          title: 'Delete',
          template: '<p>Are you sure you want to delete this shift?</p>'
        }).then(function (res) {

          if (res) {
            $ionicLoading.show({template: 'Loading...'});
            var id = $scope.modal.scope.data._id;
            sheetService.delete(id).then(function () {
              $scope.init();
              $scope.modal.remove();
            },function(){
              $ionicLoading.hide();
              $scope.modal.remove();
            });
          } else {
            $scope.modal.remove();
          }
        }, function () {
          $scope.modal.remove();
        })

      }

    });

