angular.module('hours.sheets', [
    'hours.sheets.services',
    'hours.sheets.controllers',
    'hours.sheets.router'
])
