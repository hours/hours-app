angular.module('hours.sheets.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {
    $stateProvider

      .state('tab.sheets', {
        url: '/sheets/:employeeId/:weekId',
        views: {
          'tab-sheets': {
            templateUrl: 'app/sheets/sheets.html',
            controller: 'SheetsController'
          }
        }
      })

  });
