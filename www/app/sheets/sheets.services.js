angular.module('hours.sheets.services', [
    'ionic'
  ])

  .factory('sheetService', function ($q, $http, $rootScope) {

    var that = this;

    function getDate(weekId) {
      return moment().year(weekId.substr(0, 4)).isoWeek(weekId.substr(4, 6)).startOf('isoWeek');
    }

    return {

      find: function (organisationId, employeeId, weekId) {

        var format = "YYYY-MM-DD";
        var date = getDate(weekId);

        return $q(function (resolve, reject) {


          var data = {
            date: date
          };

          // timesheet url
          var url = "/api/organisations/" + organisationId;
          if (employeeId !== "me") url += "/users/" + employeeId;
          url += "/shifts";
          if (weekId) url += "?yearweek=" + weekId;

          $http.get(url).then(function (response) {

            var total = 0;
            data.timesheets = {};

            for (i = 0; i < 7; i++) {
              data.timesheets[date.clone().add(i, 'days').format(format)] = [];
            }

            var shifts = response.data;

            shifts.forEach(function (shift) {

              var date = moment(shift.date);

              var from = moment(shift.from);
              var to = moment(shift.to);

              var diff = to.diff(from) - shift.pause;
              var dure = moment.duration(diff)

              function pad(num, size) {
                var s = num+"";
                while (s.length < size) s = "0" + s;
                return s;
              }

              var timesheet = {
                shift: shift,
                time: from.clone().format("HH:mm") + " - " + to.clone().format("HH:mm"),
                status: shift.status,
                type: shift.type,
                amount: shift.amount,
                description: shift.description,
                diff: [pad(Math.floor(dure.asHours()), 2), pad(dure.minutes(), 2) ].join(':'),
                pause: moment(shift.pause || 0).utcOffset(0).format("HH:mm")
              };

              if (shift.status !== "open" && shift.status !== "assigned")
                data.timesheets[date.format(format)].push(timesheet);

              if (shift.type === 'SHIFT' && shift.status === "approved")
                total += diff;

            });

            function pad(n) {
              n = Math.floor(n)
              return (n < 10) ? ("0" + n) : n;
            }

            var duration = moment.duration(total);
            data.total = pad(duration.asHours()) + ":" + pad(duration.minutes());

            resolve(data)

          }, reject);


        });

      },


      save: function (data) {
console.log('lalala')
console.log(data);

        return $q(function (resolve, reject) {
          if (data._id) {
            $http.put("/api/shifts/" + data._id, data).then(resolve, reject);
          } else {
            $http.post("/api/shifts", data).then(resolve, reject);
          }

        });
      },

      delete: function (id) {

        return $q(function (resolve, reject) {
          $http.delete("/api/shifts/" + id)
            .then(resolve, reject);
        });
      }
    }
  });


