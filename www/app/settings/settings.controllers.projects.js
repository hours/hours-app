angular.module('hours.settings.controllers.projects', [
    'ionic'
  ])

  .controller('SettingsProjectsController', function ($log, $scope, $http, $state, $ionicPopup, $stateParams, $ionicLoading, cacheService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });

    $scope.projects = null;

    $scope.init = function () {

      $ionicLoading.show({
        template: 'Loading...'
      });

      cacheService.organisation().then(function (organisation) {
        $http.get("/api/organisations/" + organisation._id + "/projects")
          .success(function (data) {
            $scope.projects = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
          })
          .error(function (data) {
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
          });
      })


    }

    $scope.goUsers = function (project) {
      $state.go('tab.settings-projects-users', {
        projectId: project._id
      });
    };

    $scope.add = function () {
      var popup = openPopup();

      popup.then(function (project) {
        if (project && project.name != undefined) {
          cacheService.organisation().then(function (organisation) {
            $http.post("/api/organisations/" + organisation._id + "/projects", project)
              .success(function (data) {
                $scope.init();
              });
          });
        }
      });
    };

    $scope.edit = function (project) {

      var popup = openPopup(project);

      popup.then(function (project) {
        if (project && project.name != "") {
          cacheService.organisation().then(function (organisation) {
            $http.put('/api/organisations/' + organisation._id + '/projects/' + project._id, project)
              .success(function (data) {
                $scope.init();
              });
          });
        } else $scope.init();
      });
    };


    $scope.delete = function (project) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Remove project',
        template: 'Are you sure you want to remove this project?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          cacheService.organisation().then(function (organisation) {
            $http.delete('/api/organisations/' + organisation._id + '/projects/' + project._id)
              .success(function (data) {
                $scope.init();
              });
          });
        } else {
          $scope.init();
        }
      });
    };

    var openPopup = function (project) {
      if (project) $scope.project = project;
      else $scope.project = {};
      return $ionicPopup.show({
        templateUrl: 'app/settings/settings-projects-form.html',
        title: 'Project',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!$scope.project.name) {
                (e.preventDefault());
              }
              else {
                return $scope.project;
              }

            }
          }
        ]
      });
    }

  })


  .controller('SettingsProjectsUsersController', function ($log, $q, $scope, $http, $state, $ionicPopup, $stateParams, $ionicLoading, cacheService, employeesService) {

    var projectId = $stateParams.projectId;

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });

    $scope.users = [];

    $scope.init = function () {

      $ionicLoading.show({
        template: 'Loading...'
      });

      cacheService.organisation().then(function (organisation) {
        $q.all({
          project: $http.get("/api/organisations/" + organisation._id + "/projects/" + projectId),
          employees: employeesService.findAll(organisation._id)
        }).then(function (res) {
          $scope.project = res.project.data;
          $scope.employees = res.employees;
          angular.forEach($scope.project.users || [], function (user) {
            angular.forEach($scope.employees, function (employee) {
              if (employee._id === user.id) {
                employee.checked = true;

              }
            });
          });

          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
      });
    };

    $scope.change = function () {
      var users = []
      angular.forEach($scope.employees, function (user) {
        if (user.checked) {
          users.push({
            id: user._id
          })
        }
      });
      $scope.project.users = users;
      cacheService.organisation().then(function (organisation) {
        $http.put("/api/organisations/" + organisation._id + "/projects/" + projectId, $scope.project)
      });
    }

    $scope.all = function () {
      var users = [];
      angular.forEach($scope.employees, function (user) {
        user.checked = true;
        users.push({
          id: user._id
        })
      });
      $scope.project.users = users;
      cacheService.organisation().then(function (organisation) {
        $http.put("/api/organisations/" + organisation._id + "/projects/" + projectId, $scope.project)
      });
    }
  });
