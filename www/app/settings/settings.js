angular.module('hours.settings', [
  'hours.settings.services',

  'hours.settings.controllers',
  'hours.settings.controllers.approvers',
  'hours.settings.controllers.projects',
  'hours.settings.controllers.organisations',

  'hours.settings.router'
]);
