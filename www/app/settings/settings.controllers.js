angular.module('hours.settings.controllers', [
    'ionic',
    'hours.service.credits'
  ])

  .controller('SettingsController', function ($q, $scope, $http, $log, $state, $ionicPopup, settingsService, tokenService, cacheService, analyticsService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init()
    });

    $scope.init = function () {

      $q.all({
        user: cacheService.user(),
        organisation: cacheService.organisation(),
        balance: settingsService.getBalance()
      }).then(function (res) {
        $scope.user = res.user;
        $scope.organisation = res.organisation;
        $scope.credits = res.balance.data;
      });

    };

    $scope.goUser = function () {
      $state.go("tab.settings-user")
    };

    $scope.goOrganisations = function () {
      $state.go("tab.settings-organisations")
    };

    $scope.goTransactions = function () {
      $state.go("tab.settings-transactions")
    };

    $scope.goProjects = function () {
      $state.go("tab.settings-projects")
    };

    $scope.goApprover = function () {
      $state.go("tab.settings-approver-list")
    };


    $scope.share = function () {

      analyticsService.trackEvent('linking', 'qrcode');

      var scope = $scope.$new();
      var url = "http://hours-app.com/token.html#/organisation/" + tokenService.encode($scope.organisation);
      console.log(url, encodeURIComponent(url))
      scope.qrcode = "https://chart.googleapis.com/chart?chs=150x150&cht=qr&chl=" + encodeURIComponent(url) + "&choe=UTF-8&chld=L|0";

      $ionicPopup.show({
        templateUrl: 'app/settings/settings-share.html',
        title: 'Share organisation',
        subTitle: $scope.organisation.name,
        scope: scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: '<b>Send</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (window.plugins && window.plugins.socialsharing) {
                var message = "Klik op deze link om de organisatie " + $scope.organisation.name + " toe te voegen aan de Hours app";
                var subject = $scope.organisation.name;
                window.plugins.socialsharing.share(message, subject, null, url);
                analyticsService.trackEvent('connect', 'share');
              } else {
                alert('share: ' + url);
                analyticsService.trackEvent('connect', 'share');
              }
            }
          }
        ]
      });
    };
  })

  .controller('SettingsUserController', function ($scope, $state, $http, cacheService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init()
    });

    $scope.init = function () {
      $http.get("/api/users/me").then(function (res) {
        $scope.user = res.data
      })
    }

    $scope.save = function () {
      $http.put("/api/users/me", $scope.user).then(function (res) {
        $scope.$parent.user = $scope.user;
        cacheService.flush()
        $state.go("tab.settings")
      })
    }
  })

  //employees van een project
  .controller('SettingsTransactionsController', function ($scope, $log, $http, $stateParams, $state, $ionicLoading, $ionicPopup, cacheService, creditsService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init()
    });

    $scope.init = function () {

      $ionicLoading.show({
        template: 'Loading...'
      });

      cacheService.organisation().then(function (organisation) {

        $http.get("/api/organisations/" + organisation._id + "/transactions/balance")
          .success(function (data) {
            $scope.total = data;
          });

        $http.get("/api/organisations/" + organisation._id + "/transactions")
          .success(function (data) {
            $scope.transactions = data;
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
          })
          .error(function (data) {
            $ionicLoading.hide();
            $scope.$broadcast('scroll.refreshComplete');
          });

      });
    };


    $scope.addCredits = function () {
      openPopup();
    };

    var openPopup = function () {
      var scope = $scope.$new();

      scope.buyCredits = function (credits, amount) {
        creditsService.buyCredits(credits, amount).then(function () {
          $scope.init();
          popup.close();
        });
      };

      var popup = $ionicPopup.show({
        templateUrl: 'app/settings/settings-transactions-form.html',
        title: 'Credits',
        scope: scope,
        buttons: [
          {text: 'Cancel'}
        ]
      });
    }

  });
