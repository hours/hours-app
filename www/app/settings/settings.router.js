angular.module('hours.settings.router', [
    'ionic'
  ])


  .config(function ($stateProvider) {

    $stateProvider

      .state('tab.settings', {
        url: '/settings',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings.html',
            controller: 'SettingsController'
          }
        }
      })
      .state('tab.settings-user', {
        url: '/settings/user',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-user.html',
            controller: 'SettingsUserController'
          }
        }
      })
      .state('tab.settings-organisations', {
        url: '/settings/organisations',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-organisations-list.html',
            controller: 'SettingsOrganisationsController'
          }
        }
      })
      .state('tab.settings-transactions', {
        url: '/settings/transactions',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-transactions-list.html',
            controller: 'SettingsTransactionsController'
          }
        }
      })
      .state('tab.settings-approver-list', {
        url: '/settings/approver',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-approver-list.html',
            controller: 'SettingsApproverListController'
          }
        }
      })

      .state('tab.settings-approver-form', {
        url: '/settings/approver/:employeeId',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-approver-form.html',
            controller: 'SettingsApproverFormController'
          }
        }
      })

      .state('tab.settings-projects', {
        url: '/settings/projects',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-projects-list.html',
            controller: 'SettingsProjectsController'
          }
        }
      })

      .state('tab.settings-projects-users', {
        url: '/settings/projects/:projectId/users',
        views: {
          'tab-settings': {
            templateUrl: 'app/settings/settings-projects-users.html',
            controller: 'SettingsProjectsUsersController'
          }
        }
      })

  });
