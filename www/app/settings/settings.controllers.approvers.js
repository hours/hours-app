angular.module('hours.settings.controllers.approvers', [
    'ionic'
  ])

  .controller('SettingsApproverListController', function ($scope, $log, $http, $stateParams, $state, $ionicLoading, employeesService, cacheService) {

    $scope.$on('$ionicView.loaded', function () {
      $scope.init();
    });

    $scope.init = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });
      cacheService.organisation().then(function (organisation) {
        employeesService.findAll(organisation._id).then(function (data) {
          $scope.employees = data;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        }, function (data) {
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
      });

    };

    $scope.goForm = function (employee) {
      $state.go("tab.settings-approver-form", {employeeId: employee._id})
    }

  })

  .controller('SettingsApproverFormController', function ($scope, $log, $http, $stateParams, $state, $ionicLoading, cacheService, employeesService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });

    var employeeId = $stateParams.employeeId;

    var user = null;
    var users = null;
    var checked = null;

    $scope.init = function () {

      user = null;
      users = null;
      checked = null;

      $ionicLoading.show({
        template: 'Loading...'
      });

      cacheService.organisation().then(function (organisation) {
        employeesService.findAll(organisation._id).then(function (data) {

        var user = null;
        var users = data;
        var checked = [];

        $scope.employees = [];

        //loop door users org
        for (var i = 0; i < users.length; i++) {
          if (users[i]._id == employeeId) {
            user = users[i];
          } else {
            $scope.employees.push(users[i])
          }
        }
        $scope.employee = user;
        if (!user.approver) user.approver = [];
        for (var i = 0; i < user.approver.length; i++) {
          checked[user.approver[i].id] = true;
        }

        for (var i = 0; i < users.length; i++) {
          if (checked[users[i]._id]) users[i].checked = true;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      }, function () {
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
      });
      });

    };

    $scope.change = function () {
      $ionicLoading.show({
        template: 'Loading...'
      });
      $scope.employee.approver = [];
      for (var i = 0; i < $scope.employees.length; i++) {
        if ($scope.employees[i].checked) $scope.employee.approver.push({
          id: $scope.employees[i]._id,
          active: true
        });
      }
      $http.put("/api/organisations/" + $scope.organisation._id + "/users/" + $scope.employee._id, $scope.employee).then(function () {
        $ionicLoading.hide();
      });
    }
  });

