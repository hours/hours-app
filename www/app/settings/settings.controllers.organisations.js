angular.module('hours.settings.controllers.organisations', [
    'ionic'
  ])

  .controller('SettingsOrganisationsController', function ($log, $rootScope, $scope, $http, $state, $ionicPopup, $stateParams, $ionicLoading, storageService, barcodeService, organisationService, cacheService) {

    $scope.$on('$ionicView.beforeEnter', function () {
      $scope.init();
    });

    $scope.organisation = null;

    $scope.organisations = [];

    $scope.init = function () {

      $ionicLoading.show({
        template: 'Loading...'
      });

      $scope.organisations = {};

      organisationService.findAll().then(function (data) {
          if (data.length === 0) {
            storageService.removeOrganisation().then(function () {
              cacheService.flush();
              $state.go('setup');
            });
          }

          $scope.organisations = data;
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        },
        function () {
          $ionicLoading.hide();
          $scope.$broadcast('scroll.refreshComplete');
        });
    };

    $scope.scan = function () {
      barcodeService.scanQrcode().then(function (res) {
        if(res.action === 'organisation'){
          $state.go('token-organisation', {
            token: res.token
          });
        }
      }, function (err) {
        $ionicPopup.alert({
          title: 'QR code',
          template: 'Cannot read QR code.'
        });
      });
    };

    $scope.add = function () {
      var organisation = {}
      var popup = openPopup(organisation);

      popup.then(function (organisation) {
        if (organisation && organisation.name != undefined) {
          $http.post("/api/organisations", organisation)
            .success(function () {
              $scope.init();
            });
        }
      });
    };

    $scope.edit = function (organisation) {
      var popup = openPopup(organisation);

      popup.then(function (organisation) {
        if (organisation && organisation.name != "") {
          $http.put("/api/organisations/" + organisation._id, organisation)
            .then(function (res) {
              cacheService.flush()
              $rootScope.organisation = res.data;
              $scope.init();
            });
        } else $scope.init();
      });
    };

    $scope.delete = function (organisation) {
      var confirmPopup = $ionicPopup.confirm({
        title: 'Remove organisation',
        template: 'Are you sure you want to remove this organisation?'
      });
      confirmPopup.then(function (res) {
        if (res) {
          organisationService.remove(organisation).then(function () {
            storageService.getOrganisation().then(function (id) {
              if (organisation._id === id) {
                storageService.removeOrganisation().then(function () {
                  $scope.init();
                });
              } else {
                $scope.init();
              }
            });

          });
        }
      });
    };

    $scope.projects = function (organisation) {
      $state.go("tab.organisations-projects", {"organisationId": organisation._id})
    };

    $scope.select = function (organisation) {
      storageService.setOrganisation(organisation._id);
      cacheService.flush();
      $state.go('tab.settings');
    };

    var openPopup = function (organisation) {
      var scope = $scope.$new();
      scope.organisation = organisation;
      return $ionicPopup.show({
        templateUrl: 'app/settings/settings-organisations-form.html',
        title: 'Organisation',
        scope: scope,
        buttons: [
          {text: 'Close'},
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function (e) {
              if (!scope.organisation.name) {
                e.preventDefault();
              }
              else {
                return scope.organisation;
              }
            }
          }
        ]
      });
    }
  });
