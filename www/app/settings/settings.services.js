angular.module('hours.settings.services', [
    'ionic'
  ])

  .factory('settingsService', function ($http, $q, storageService, cacheService, organisationService) {

    return {

      getUser: function () {
        return $http.get("/api/users/me")
      },

      getOrganisation: function () {
        return $q(function (resolve, reject) {
          storageService.getOrganisation().then(function (organisationId) {
            organisationService.find(organisationId).then(resolve, reject)
          })
        })

      },
      getBalance: function () {
        return cacheService.organisation().then(function (organisation) {
          return $http.get("/api/organisations/" + organisation._id + "/transactions/balance")
        })
      }
    }

  });
