angular.module('hours.service.token', [])

  .factory("tokenService", function () {

    return {
      encode: function (organisation) {
        var obj = {
          id: organisation._id,
          name: organisation.name
        }
        return btoa(JSON.stringify(obj));
      },

      decode: function (token) {
        return JSON.parse(atob(token));
      }
    }

  });
