// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', [
    'ionic',
    'hours',
    'ngIOS9UIWebViewPatch',
  ])


  .constant("LOGIN_URL", function () {
    return LOGIN_URL;
  }())

  .constant("API_URL", function () {
    if (window.cordova)
      return API_URL;
    else
      return ""
  }())

  .config(function ($ionicConfigProvider) {
    $ionicConfigProvider.backButton.text('').previousTitleText(false);
  })

  .run(function ($ionicPlatform) {
    $ionicPlatform.ready(function () {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      }
      if (window.StatusBar) {
        window.StatusBar.styleDefault();
      }
    });
  })

  .run(function ($ionicPlatform) {

    $ionicPlatform.ready(function () {
      $ionicPlatform.on('resume', function () {
        console.log('resume resume resume resume')
      });
    });
  })

  .config(function ($stateProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

    // setup an abstract state for the tabs directive


    // Each tab has its own nav history stack:
      .state('loading', {
        url: '/loading',
        templateUrl: 'templates/loading.html',
        controller: 'LoadingCtrl'
      })


      // Each tab has its own nav history stack:


      .state('tab.employee-timesheet', {
        url: '/organisations/:organisationId/:employeeId/:weekId',
        views: {
          'tab-organisations': {
            templateUrl: 'templates/timesheet.html',
            controller: 'TimesheetCtrl'
          }
        }
      })

      .state('tab.share', {
        url: '/share/:organisationId',
        views: {
          'tab-organisations': {
            templateUrl: 'templates/share.html',
            controller: 'ShareController'
          }
        }
      })

  });
