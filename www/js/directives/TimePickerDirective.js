//By Rajeshwar Patlolla
//https://github.com/rajeshwarpatlolla

angular.module('starter')

.directive('timePicker', function() {
    return {
        restrict: 'E',
        templateUrl: 'templates/timesheet-timepicker.html',
        scope: {
          date:'=date'
        },
        controller: function ($scope) {

          $scope.increaseHours = function ($event) {
              $scope.date.setHours ($scope.date.getHours() + 1 );
              $event.stopPropagation();
          };

          $scope.decreaseHours = function ($event) {
              $scope.date.setHours ($scope.date.getHours() - 1 );
              $event.stopPropagation();
          };

          $scope.increaseMinutes = function ($event) {
              $scope.date.setMinutes ($scope.date.getMinutes() + 15 );
              $event.stopPropagation();
          };

          $scope.decreaseMinutes = function ($event) {
              $scope.date.setMinutes ($scope.date.getMinutes() - 15 );
              $event.stopPropagation();
          };
      }
    };
});
