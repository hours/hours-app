var express = require('express');
var bodyParser = require('body-parser');

var request = require('request');

var port = process.env.PORT || 8888;

//var API_URL = process.env.API_URL || "http://localhost:3000";
var API_URL = process.env.API_URL || "https://hours-api-test.herokuapp.com";
var LOGIN_URL = process.env.LOGIN_URL || "https://hours-app-test.herokuapp.com?redirect=app";

var app = express();

app.use("/", express.static(__dirname + "/www"));

app.use(bodyParser.json());

app.all("/api/*", function (req, res, next) {
  var options = {
    method: req.method,
    url: API_URL + req.path,
    qs: req.query,
    headers: {
      authorization: req.headers.authorization
    },
    json: true,
    body: req.body
  };
  request(options)
    .on('error', function (err) {
      next(err);
    })
    .pipe(res);
});

app.get("/auth/*", function (req, res) {
  var obj = url.parse(LOGIN_URL);
  obj.pathname = req.path;
  res.redirect(url.format(obj));
});

app.get("/login", function (req, res) {
  res.redirect(LOGIN_URL);
});


app.listen(port, function () {
  console.log("Node app is running at localhost:" + port);
});
