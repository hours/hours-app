#!/usr/bin/env node

var fs = require('fs');
var path = require('path');

var global = '';
if (process.env.CORDOVA_CMDLINE && process.env.CORDOVA_CMDLINE.indexOf('--release') >= 0) {
  global += "var API_URL = 'https://hours-api-v6.herokuapp.com';" + "\n";
  global += "var LOGIN_URL = 'https://hours-api-v6.herokuapp.com';" + "\n";
} else {
  global += "var API_URL = 'https://hours-api-test.herokuapp.com';" + "\n";
  global += "var LOGIN_URL = 'https://hours-api-test.herokuapp.com';" + "\n";
}

fs.writeFile(__dirname + "/../../www/js/global.js", global, function(err) {
  if(err) {
    return console.log(err);
  }
  console.log("The file was saved!");
});
